using System;
using System.Linq.Expressions;
using SQLite;

namespace Ludvig.DataAccess.Databases
{
	public class SqLiteNetQueryBuilder<T> : IQueryBuilder<T>
	{
		public TableQuery<T> TableQuery { get; private set; }

		public SqLiteNetQueryBuilder(TableQuery<T> tableQuery)
		{
			TableQuery = tableQuery;
		}

		public IQueryBuilder<T> Where(Expression<Func<T, bool>> action)
		{
			TableQuery = TableQuery.Where(action);
			return this;
		}

		public IQueryBuilder<T> Take(int limit)
		{
			TableQuery = TableQuery.Take(limit);
			return this;
		}

		public IQueryBuilder<T> Skip(int amount)
		{
			TableQuery = TableQuery.Skip(amount);
			return this;
		}

		public IQueryBuilder<T> OrderBy(params Expression<Func<T, object>>[] fields)
		{
			foreach (var field in fields)
			{
				TableQuery = TableQuery.OrderBy(field);
			}
			
			return this;
		}

		public IQueryBuilder<T> OrderByDescending(params Expression<Func<T, object>>[] fields)
		{
			foreach (var field in fields)
			{
				TableQuery = TableQuery.OrderByDescending(field);
			}

			return this;
		}

		public IQueryBuilder<T> Select(params Expression<Func<T, object>>[] fields)
		{
			// not supported yet!
			foreach (var field in fields)
			{
				//_tableQuery = _tableQuery.Select(field);
			}
			return this;
		}
	}
}