﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Ludvig.DataAccess.Databases
{
	public interface IWarehouse<T> where T : class
	{
		List<T> Query(Action<IQueryBuilder<T>> queryManipulator);
		List<T> QueryAll();

		long Count(Expression<Func<T, bool>> action = null);

		void Add(T t);
		void Save(T t);
		void Update(T t);
		void Delete(T t);
	}
}
