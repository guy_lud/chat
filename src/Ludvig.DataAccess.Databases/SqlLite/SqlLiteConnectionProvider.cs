﻿using System;
using SQLite;

namespace Ludvig.DataAccess.Databases.SqlLite
{
	public class SqlLiteConnectionProvider : ISqlConnectionProvider
	{
		private readonly string _path;

		public SqlLiteConnectionProvider(string path)
		{
			if (path == null) throw new ArgumentNullException(nameof(path));

			_path = path;
		}

		public SQLiteConnection GetConnection()
		{
			//var connection = $"Source = {_path}; Pooling = true";

			return new SQLiteConnection(_path);

			//return SQLiteConnectionPool.Shared.GetConnection(new SQLiteConnectionString(_path, false), SQLiteOpenFlags.ReadWrite);
		}
	}
}