﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using SQLite;

namespace Ludvig.DataAccess.Databases.SqlLite
{
	public class SqlLiteWarehouse<T> : IWarehouse<T> where T : class, new()
	{
		private readonly ISqlConnectionProvider _sqlConnectionProvider;

		private SQLiteConnection Connection
		{
			get { return _sqlConnectionProvider.GetConnection(); }
		}

		public SqlLiteWarehouse(ISqlConnectionProvider sqlConnectionProvider)
		{
			_sqlConnectionProvider = sqlConnectionProvider;
		}


		public List<T> Query(Action<IQueryBuilder<T>> queryManipulator)
		{
			using(var con = Connection)
			{
				var query = con.Table<T>();
				var queryBuilder = new SqLiteNetQueryBuilder<T>(query);
				queryManipulator(queryBuilder);
				return queryBuilder.TableQuery.ToList();
			}
		}

		public List<T> QueryAll()
		{
			using (var con = Connection)
			{
				return con.Table<T>().ToList();
			}
		}

		public long Count(Expression<Func<T, bool>> action = null)
		{
			using (var con = Connection)
			{
				return action == null ? con.Table<T>().Count() : con.Table<T>().Count(action);
			}
		}

		public void Add(T t)
		{
			using (var con = Connection)
			{
				con.Insert(t);
			}
		}

		public void Save(T t)
		{
			using (var con = Connection)
			{
				con.InsertOrReplace(t);
			}
		}

		public void Update(T t)
		{
			using (var con = Connection)
			{
				con.Update(t);
			}
		}

		public void Delete(T t)
		{
			using (var con = Connection)
			{
				con.Delete<T>(t);
			}
		}
	}
}
