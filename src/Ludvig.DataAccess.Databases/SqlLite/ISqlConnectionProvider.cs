﻿using System.Data;
using SQLite;

namespace Ludvig.DataAccess.Databases.SqlLite
{
	public interface ISqlConnectionProvider
	{
		SQLiteConnection GetConnection();
	}
}