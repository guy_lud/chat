﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hermes.Data.Model.AffiliateNetworks;

namespace Hermes.Data.DAL.DataStores
{
    public class Elastic : ISearchEngine
    {
        public Task IndexOfferAsync(Offer offer)
        {
            return Task.Delay(1);
        }

        public Task ConnectAsync()
        {
            return Task.Delay(1);
        }
    }
}
