﻿using Hermes.Data.Model.AffiliateNetworks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hermes.Data.DAL.DataStores
{
    public class Mongo : IDatabase
    {
        private MongoClient client;
        private IMongoDatabase database;
        private IMongoCollection<Offer> offers;
        private ThreadLocal<int> count;

        public Mongo()
        {
            count = new ThreadLocal<int>(true);
        }

        public Task ConnectAsync()
        {
            return Task.Run(() =>
            {
                var url = ConfigurationManager.ConnectionStrings["mongo"].ConnectionString;
                if (string.IsNullOrEmpty(url))
                    throw new ConfigurationErrorsException("MongoDB connection string not found.");
                var settings = MongoClientSettings.FromUrl(MongoUrl.Create(url));
                settings.WaitQueueSize = 10000;
                settings.MinConnectionPoolSize = 1;
                settings.MaxConnectionPoolSize = 10;

                client = new MongoClient(settings);
                database = client.GetDatabase("camops");
                offers = database.GetCollection<Offer>("offers");
            });
        }

        public Task UpsertOfferAsync(Offer offer)
        {
            return offers.ReplaceOneAsync(doc => doc.Id == offer.Id, offer, new UpdateOptions { IsUpsert = true }).ContinueWith(t => {
                Console.WriteLine(t.Result.ToString());
                count.Value++;
            });
        }

        public void Debug()
        {
            Console.WriteLine(count.Values.Sum().ToString());
        }
    }
}
