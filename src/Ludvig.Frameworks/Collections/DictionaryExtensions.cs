﻿using System.Collections.Generic;

namespace Hermes.Frameworks.Collections
{
	public static class DictionaryExtensions
	{
		public static TValue ItemOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> target,
			TKey key,
			TValue @default = default(TValue))
		{
			TValue value;
			return target.TryGetValue(key, out value) ? value : @default;
		}
	}
}
