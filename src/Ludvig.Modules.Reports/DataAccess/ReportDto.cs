﻿using System;
using SQLite;

namespace Ludvig.Modules.Reports.DataAccess
{
	[Table("Reports")]
	internal class ReportDto
	{
		public string Type { get; set; }
		public string Data { get; set; }
		public DateTime TimeCreated { get; set; }
	}
}
