﻿using System.Linq;
using Ludvig.DataAccess.Databases;
using Ludvig.Modules.Contracts.Reports;
using Ludvig.Utilities.DateTime;
using Ludvig.Utilities.Defence;
using Newtonsoft.Json;

namespace Ludvig.Modules.Reports.DataAccess
{
	internal class ReportRepository : IReportRepository
	{
		private readonly IWarehouse<ReportDto> _warehouse;
		private readonly ISystemTime _systemTime;

		private static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
		{
			DefaultValueHandling = DefaultValueHandling.Ignore,
			NullValueHandling = NullValueHandling.Ignore,
			ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
			MissingMemberHandling = MissingMemberHandling.Ignore,
			PreserveReferencesHandling = PreserveReferencesHandling.Objects,
			DateFormatHandling = DateFormatHandling.IsoDateFormat
		};

		public ReportRepository(IWarehouse<ReportDto> warehouse, ISystemTime systemTime)
		{
			_warehouse = warehouse;
			_systemTime = systemTime;
		}

		public Report<T> GetReport<T>(string type) where T : class, new()
		{
			type.ThrowIfNull();

			var dto = _warehouse.Query(q => q.Where(x => x.Type == type.ToLower()))
				.FirstOrDefault();

			if (dto == null)
				return null;

			var data = JsonConvert.DeserializeObject<T>(dto.Data, SerializerSettings);

			return new Report<T>()
			{
				Type = dto.Type,
				Data = data,
				TimeCreated = dto.TimeCreated
			};
		}

		public void SaveReport<T>(string type, T data)
		{
			type.ThrowIfNull();
			data.ThrowIfNull();

			var dto = new ReportDto
			{
				Type = type.ToLower(),
				TimeCreated = _systemTime.Now,
				Data = JsonConvert.SerializeObject(data, SerializerSettings)
			};

			_warehouse.Save(dto);
		}
	}
}
