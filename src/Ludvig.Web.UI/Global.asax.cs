﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Ludvig.Infrastructure.Bootstrap;
using Ludvig.Infrastructure.Bootstrap.Warmup;
using Ludvig.Utilities.Configuration;
using Ludvig.Utilities.Ioc;
using Ludvig.Web.UI.Infrastructure.Configuration;
using SimpleInjector;

namespace Ludvig.Web.UI
{
	public class MvcApplication : HttpApplication
	{
		private Container Container { get; } = new Container();
		private IHostingEnvironment _hostingEnvironment;
		private readonly string _rootPath = System.Web.Hosting.HostingEnvironment.MapPath("/");

		protected void Application_Start()
		{
			_hostingEnvironment = new HostingEnvironment(new SystemConfigurationManager(), _rootPath); 

			Container.Options.LifestyleSelectionBehavior = new SingletonLifestyleSelectionBehavior();
			Container.RegisterSingleton(typeof(IHostingEnvironment),() => _hostingEnvironment);
			Container.RegisterSingleton(typeof(IConfigurationManager), () => _hostingEnvironment.Configuration);

			var modules = new ModuleCollection();

			var assemblies = AssemblyCollection.New()
				.AddFromType<MvcApplication>()
				.AddModules(HttpRuntime.BinDirectory);

			var bootstrapper = new Bootstrapper(Container);

			modules.AddBasicModules(_hostingEnvironment)
				.AddMvc()
				.AddWebApi();

			var resolver = bootstrapper.Initialize(assemblies, modules);

			Container.Verify();

			var appBuilder = new ApplicationBuilder(resolver, _hostingEnvironment);

			GlobalServiceResolver.SetResolver(resolver);

			appBuilder.UseBasicModules()
				.UseMvc()
				.UseWebApi();

			appBuilder.Build();

			var warmupTasks = Container.GetAllInstances<IWarmupTask>();
			Task.WaitAll(warmupTasks.Select(x => x.Run()).ToArray());
		}
	}
}
