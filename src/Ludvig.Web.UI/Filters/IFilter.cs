﻿using System.Web.Http.Filters;
using Ludvig.Utilities.Ioc;

namespace Ludvig.Web.UI.Filters
{
	[RegisterAll]
	public interface IGlobalFilter : IFilter
	{
		
	}

	public interface IGlobalAuthenticationFilter : IGlobalFilter, IAuthenticationFilter, System.Web.Mvc.Filters.IAuthenticationFilter
	{
		
	}
}