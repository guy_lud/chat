using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using Ludvig.Modules.Contracts.Users;
using Ludvig.Utilities.Caching;
using Ludvig.Utilities.Defence;
using Ludvig.Web.UI.Infrastructure;

namespace Ludvig.Web.UI.Filters
{
	public class GeneralAuthenticationFilter : IGlobalAuthenticationFilter
	{
		public bool AllowMultiple => false;

		private const string CacheKey = "LoggedInUser";
		private const string SessionCookieName = "sid";

		private readonly IUserAuthenticator _userAuthenticator;
		private readonly IRequestLevelCacher _requestLevelCacher;

		public GeneralAuthenticationFilter(IUserAuthenticator userAuthenticator,
			IRequestLevelCacher requestLevelCacher)
		{
			_userAuthenticator = userAuthenticator;
			_requestLevelCacher = requestLevelCacher;
		}

		public void OnAuthentication(AuthenticationContext filterContext)
		{
			var actionAttributes = filterContext?.ActionDescriptor?.GetCustomAttributes(typeof(AllowAnonymousSessions),
				true);

			var controllerAttributes =
				filterContext?.ActionDescriptor?.ControllerDescriptor.GetCustomAttributes(
					typeof(AllowAnonymousSessions), true);

			OnAuthentication(!actionAttributes.IsNullOrEmpty() || !controllerAttributes.IsNullOrEmpty(), () =>
			{
				var returnUrl = "/".Equals(filterContext.HttpContext.Request.RawUrl)
					? "/hello"
					: $"/hello?returnUrl={filterContext.HttpContext.Request.RawUrl}";
				filterContext.Result = new RedirectResult(returnUrl);
			}, filterContext.HttpContext.Request.RawUrl);
		}

		public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
		{
		}

		public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
		{
			var actionAttributes =
				context.ActionContext?.ActionDescriptor?.GetCustomAttributes<AllowAnonymousSessions>(true);

			var controllerAttributes =
				context?.ActionContext?.ActionDescriptor?.ControllerDescriptor
					.GetCustomAttributes<AllowAnonymousSessions>(true);

			OnAuthentication(!actionAttributes.IsNullOrEmpty() || !controllerAttributes.IsNullOrEmpty(),
				() => { context.ErrorResult = new AuthenticationFailureResult("", context.Request); }, context.Request.RequestUri.ToString());

			return Task.CompletedTask;
		}

		public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
		{
			return Task.CompletedTask;
		}

		private void OnAuthentication(bool allowAnonymousSessions, Action next, string requestUrl)
		{
			var user = GetUser();
			if (user != null)
			{
				_requestLevelCacher.Save(CacheKey, user);
				return;
			}

			if (!allowAnonymousSessions)
			{
				next();
			}
		}

		private User GetUser()
		{
			var conext = HttpContext.Current;
			var sidCookie = conext.Request.Cookies[SessionCookieName];
			if (sidCookie == null || sidCookie.Value.IsNullOrEmpty())
				return null;

			var user = _userAuthenticator.GetSessionUser(sidCookie.Value);
			if (user == null)
				HttpContext.Current.Response.Cookies.Remove(SessionCookieName);

			return user;
		}
	}
}