﻿using System.Web.Mvc;
using Ludvig.Web.UI.Infrastructure.Rendering;

namespace Ludvig.Web.UI
{
	internal class EnginesViewBootstraper
	{
		public static void Bootstrap(ViewEngineCollection viewEngineCollection)
		{
			viewEngineCollection.Clear();
			viewEngineCollection.Add(new ReaperViewEngine());
		}
	}
}