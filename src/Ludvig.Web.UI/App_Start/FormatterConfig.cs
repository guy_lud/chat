﻿using System.Net.Http.Formatting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Ludvig.Web.UI
{
	public class FormatterConfig
	{
		public static void RegisterFormatters(MediaTypeFormatterCollection formatters)
		{
			formatters.Clear();

			var jsonFormatter = new JsonMediaTypeFormatter();
			var settings = jsonFormatter.SerializerSettings;
			settings.Formatting = Formatting.Indented;
			settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

			formatters.Add(jsonFormatter);
			formatters.Add(new FormUrlEncodedMediaTypeFormatter());
		}
	}
}