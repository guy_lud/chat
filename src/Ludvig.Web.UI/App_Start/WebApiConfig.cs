﻿using System.Web.Http;

namespace Ludvig.Web.UI
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			config.MapHttpAttributeRoutes();
			FormatterConfig.RegisterFormatters(config.Formatters);
		}
	}
}
