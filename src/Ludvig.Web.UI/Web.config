﻿<?xml version="1.0" encoding="utf-8"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  http://go.microsoft.com/fwlink/?LinkId=301880
  -->
<configuration>
	<configSections>
		<!-- For more information on Entity Framework configuration, visit http://go.microsoft.com/fwlink/?LinkID=237468 -->
		<sectionGroup name="bundleTransformer">
			<section name="core" type="BundleTransformer.Core.Configuration.CoreSettings, BundleTransformer.Core" />
			<section name="sassAndScss" type="BundleTransformer.SassAndScss.Configuration.SassAndScssSettings, BundleTransformer.SassAndScss" />
		</sectionGroup>
	</configSections>
	<appSettings file="appSettings.config">
		<add key="webpages:Version" value="3.0.0.0" />
		<add key="webpages:Enabled" value="false" />
		<add key="ClientValidationEnabled" value="true" />
		<add key="UnobtrusiveJavaScriptEnabled" value="true" />
		<add key="ChatDBPath" value="..\..\database\chat.db" />
	</appSettings>
	<!--
    For a description of web.config changes see http://go.microsoft.com/fwlink/?LinkId=235367.

    The following attributes can be set on the <httpRuntime> tag.
      <system.Web>
        <httpRuntime targetFramework="4.6" />
      </system.Web>
  -->
	<system.web>
		<authentication mode="None" />
		<compilation debug="true" targetFramework="4.6" />
		<httpRuntime targetFramework="4.6" />
		<customErrors mode="Off" />
	</system.web>
	<system.webServer>
		<modules>
			<remove name="FormsAuthentication" />
		</modules>
		<handlers>
			<clear />
			<add name="JavaScriptFiles" path="*.js" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="FlashFiles" path="*.swf" verb="*" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="VideoFiles" path="*.mp4" verb="*" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="HtmlPages" path="*.html" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="HtmPages" path="*.htm" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="GifImages" path="*.gif" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="PngImages" path="*.png" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="JpegImages" path="*.jpg" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="CssFiles" path="*.css" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="CssMapFiles" path="*.css.map" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="ScssFiles" path="*.scss" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="Icons" path="*.ico" verb="*" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="XmlFiles" path="*.xml" verb="*" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="TextFiles" path="*.txt" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="PdfFiles" path="*.pdf" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="SvgFiles" path="*.svg" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="TtfFiles" path="*.ttf" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="EotFiles" path="*.eot" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="WoffFiles" path="*.woff" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="Woff2Files" path="*.woff2" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="OtfFiles" path="*.otf" verb="GET" modules="StaticFileModule" scriptProcessor="" resourceType="File" requireAccess="Script" />
			<add name="BlockViewHandler" path="*" verb="*" preCondition="integratedMode" type="System.Web.HttpNotFoundHandler" />
			<add name="SassAssetHandler" path="*.sass" verb="GET" type="BundleTransformer.SassAndScss.HttpHandlers.SassAndScssAssetHandler, BundleTransformer.SassAndScss" resourceType="File" preCondition="" />
			<add name="ScssAssetHandler" path="*.scss" verb="GET" type="BundleTransformer.SassAndScss.HttpHandlers.SassAndScssAssetHandler, BundleTransformer.SassAndScss" resourceType="File" preCondition="" />
		</handlers>
		<staticContent>
			<!--Workaround for IIS Express already having all MIME maps defined.-->
			<remove fileExtension=".woff" />
			<mimeMap fileExtension=".woff" mimeType="application/x-woff" />
			<remove fileExtension=".woff2" />
			<mimeMap fileExtension=".woff2" mimeType="application/x-woff2" />
		</staticContent>
		<security>
			<requestFiltering>
				<hiddenSegments>
					<add segment="LibSassHost.Native" />
				</hiddenSegments>
			</requestFiltering>
		</security>
	</system.webServer>
	<runtime>
		<assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
			<dependentAssembly>
				<assemblyIdentity name="Newtonsoft.Json" culture="neutral" publicKeyToken="30ad4fe6b2a6aeed" />
				<bindingRedirect oldVersion="0.0.0.0-9.0.0.0" newVersion="9.0.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Web.Optimization" publicKeyToken="31bf3856ad364e35" />
				<bindingRedirect oldVersion="1.0.0.0-1.1.0.0" newVersion="1.1.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35" />
				<bindingRedirect oldVersion="0.0.0.0-1.6.5135.21930" newVersion="1.6.5135.21930" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35" />
				<bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
				<bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35" />
				<bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Web.Http" publicKeyToken="31bf3856ad364e35" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Common.Logging.Core" publicKeyToken="af08829b84f0328e" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-3.3.1.0" newVersion="3.3.1.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Common.Logging" publicKeyToken="af08829b84f0328e" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-3.3.1.0" newVersion="3.3.1.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="log4net" publicKeyToken="669e0ddf0bb1aa2a" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-1.2.15.0" newVersion="1.2.15.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="LibSassHost" publicKeyToken="3e24e88796a38e46" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-0.5.2.0" newVersion="0.5.2.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="SimpleInjector" publicKeyToken="984cb50dea722e99" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-3.2.2.0" newVersion="3.2.2.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Antlr3.Runtime" publicKeyToken="eb42632606e9261f" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-3.5.0.2" newVersion="3.5.0.2" />
			</dependentAssembly>
		</assemblyBinding>
	</runtime>
	<bundleTransformer xmlns="http://tempuri.org/BundleTransformer.Configuration.xsd">
		<core>
			<css>
				<translators>
					<add name="NullTranslator" type="BundleTransformer.Core.Translators.NullTranslator, BundleTransformer.Core" enabled="false" />
					<add name="SassAndScssTranslator" type="BundleTransformer.SassAndScss.Translators.SassAndScssTranslator, BundleTransformer.SassAndScss" />
				</translators>
				<postProcessors>
					<add name="UrlRewritingCssPostProcessor" type="BundleTransformer.Core.PostProcessors.UrlRewritingCssPostProcessor, BundleTransformer.Core" useInDebugMode="false" />
				</postProcessors>
				<minifiers>
					<add name="NullMinifier" type="BundleTransformer.Core.Minifiers.NullMinifier, BundleTransformer.Core" />
				</minifiers>
				<fileExtensions>
					<add fileExtension=".css" assetTypeCode="Css" />
					<add fileExtension=".sass" assetTypeCode="Sass" />
					<add fileExtension=".scss" assetTypeCode="Scss" />
				</fileExtensions>
			</css>
			<js>
				<translators>
					<add name="NullTranslator" type="BundleTransformer.Core.Translators.NullTranslator, BundleTransformer.Core" enabled="false" />
				</translators>
				<minifiers>
					<add name="NullMinifier" type="BundleTransformer.Core.Minifiers.NullMinifier, BundleTransformer.Core" />
				</minifiers>
				<fileExtensions>
					<add fileExtension=".js" assetTypeCode="JavaScript" />
				</fileExtensions>
			</js>
		</core>
	</bundleTransformer>
</configuration>