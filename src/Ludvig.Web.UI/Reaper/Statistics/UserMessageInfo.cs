﻿namespace Ludvig.Web.UI.Reaper.Statistics
{
	public class UserMessageInfo
	{
		public string UserName { get; private set; }
		public float AvrageMessageLength { get; private set; }

		public UserMessageInfo(string userName, float avrageMessageLength)
		{
			UserName = userName;
			AvrageMessageLength = avrageMessageLength;
		}
	}
}