﻿namespace Ludvig.Web.UI.Reaper.Statistics
{
	public class MessageLengthStatsModel
	{
		public float MessageLengthAvrage { get; set; }
		public UserMessageInfo[] UsersStats { get; set; } = new UserMessageInfo[0];
	}
}