﻿define(['jquery', 'knockout', 'pubsub'],
	function ($, ko, pubsub) {
		return function () {
			var self = this;

			self.init = function () {
				initTimer();
			};

			function fetchMessageTimeAvrage() {

				var promise = $.ajax({
					url: '/statistics/fetch-avrage-message-time',
					method: 'GET',
					contentType: 'application/json'
				});

				promise.done(function (data) {
					pubsub.publish('avrage_time', data);

				})
					.fail(function () {
						pubsub.publish('avrage_time', 0);
					});

			};

			function initTimer() {
				if (self.timer != null)
					clearTimeout(self.timer);
				self.timer = setInterval(function () {
					fetchMessageTimeAvrage();
				}, 5000);
			};

		};
	});