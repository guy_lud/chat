﻿using System.Linq;
using Ludvig.Modules.Contracts.Reports;
using Ludvig.Modules.Contracts.Users;

namespace Ludvig.Web.UI.Reaper.Statistics
{
	public interface IMessageLengthStatsModelCreator
	{
		MessageLengthStatsModel CreateModel(MessagesAvrageLengthReport report);
	}

	internal class MessageLengthStatsModelCreator : IMessageLengthStatsModelCreator
	{
		private readonly IUserRepository _userRepository;

		public MessageLengthStatsModelCreator(IUserRepository userRepository)
		{
			_userRepository = userRepository;
		}

		public MessageLengthStatsModel CreateModel(MessagesAvrageLengthReport report)
		{
			var model = new MessageLengthStatsModel
			{
				MessageLengthAvrage = report.Avg,
				UsersStats = report.UserMessageWordsAvragesCollection
					.Select(x =>
					{
						var user = _userRepository.GetUserById(x.UserId);
						return new UserMessageInfo(user.UserName, x.LastAvrage);
					}).ToArray()
			};

			return model;
		}
	}
}