﻿define(['jquery',
	'knockout',
	'pubsub',
	'avrage-message-time-publisher',
	'avrage-message-length-publisher'],
	function ($, ko, pubsub, amtPubliser, amlPublisher) {
		return function () {
			var self = this;

			init();

			self.avrageMessageTime = ko.observable();
			self.avrageMessageLength = ko.observable();
			self.usersMessagesStats = ko.observableArray([]);

			function init() {
				pubsub.subscribe('avrage_time',
					function (msg, data) {
						self.avrageMessageTime(data);
					});

				pubsub.subscribe('avrage_message_length_stats_event',
					function (msg,data) {
						if (!data)
							return;
						
						self.avrageMessageLength(data.messageLengthAvrage);
						self.usersMessagesStats(data.usersStats);
					});

				var amt = new amtPubliser();
				amt.init();
				var aml = new amlPublisher();
				aml.init();
			};

		};
	});