﻿using System;
using System.Web.Http;
using Ludvig.Modules.Contracts.Reports;

namespace Ludvig.Web.UI.Reaper.Statistics
{
	public class StatsController : ApiController
	{
		private readonly IReportRepository _reportRepository;
			private readonly IMessageLengthStatsModelCreator _messageLengthStatsModelCreator;

		public StatsController(IReportRepository reportRepository,
			IMessageLengthStatsModelCreator messageLengthStatsModelCreator)
		{
			_reportRepository = reportRepository;
			_messageLengthStatsModelCreator = messageLengthStatsModelCreator;
		}

		[HttpGet]
		[Route("statistics/fetch-avrage-message-time")]
		public string GetAvarageMessateTime()
		{
			var report = _reportRepository.GetReport<MessageAvrageTimeReport>();

			return report?.Data == null ? "No data yet" : TimeSpan.FromTicks(report.Data.Avg).ToString();
		}

		[HttpGet]
		[Route("statistics/fetch-avrage-message-length")]
		public MessageLengthStatsModel GetAvarageLengthStats()
		{
			var report = _reportRepository.GetReport<MessagesAvrageLengthReport>();

			return report?.Data == null ? null : _messageLengthStatsModelCreator.CreateModel(report.Data);
		}
	}
}