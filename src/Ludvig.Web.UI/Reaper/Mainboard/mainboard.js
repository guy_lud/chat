﻿define(['chat','statistics'], function(chat, stats) {

	var module = {};

	module.chatModel = new chat();
	module.statisticsModal = new stats();
	return module;
});