﻿define(['plugins/router',
	'global-loading',
	'knockout',
	'ajax-calls-configurator',
	'spaClientProvider'], function (router, gl, ko, aj, spaClientProvider) {
		return {
			that: this,
			router: router,
			name: spaClientProvider.name,
			isDebug: true,
			activate: function () {
				aj.configure();

				router.map([
					{ route: ['', '!', '!/', '!/*details'], title: 'Home', moduleId: 'app-shell' }
				]);

				router.mapUnknownRoutes('error-page', '!/not-found');
				router.buildNavigationModel();
				return router.activate();
			},
			logout: function () {
				document.cookie = 'sid=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
				location.reload();
			},
			
			loading: ko.computed(function () {
				return gl.isLoading() || router.isNavigating();
			})
		};
	});