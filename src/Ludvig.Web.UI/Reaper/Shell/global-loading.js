﻿define(['knockout'], function(ko) {
	var model = {};
	model.isLoading = ko.observable(false);
	return model;
});