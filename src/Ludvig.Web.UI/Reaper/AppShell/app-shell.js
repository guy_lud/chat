﻿define(['plugins/router', 'appShellSwitcher'], function (router, switcher) {
	var childRouter = router.createChildRouter();
	childRouter.map([
		{ route: '', title: 'Home', moduleId: 'welcome' },
		{ route: '!/mainboard', title: 'Chat', moduleId: 'mainboard' },
		{ route: '!/error', title: 'Error', moduleId: 'error-page', nav: false }
	]);

	childRouter.mapUnknownRoutes('error-page', '!/not-found');

	childRouter.activeItem.settings.areSameItem = function () { return false; };

	var nav = childRouter.buildNavigationModel();

	var ctor = function () {
		this.router = nav;
		this.switcher = switcher;
	};

	return ctor;
});