﻿define(['knockout'], function (ko) {

	var module = {};

	module.displayAlternative = ko.observable(false);

	module.alternativeAppShell = ko.observable();

	module.switchShell = function (appShell) {
		module.displayAlternative(true);
		module.alternativeAppShell(appShell);
	};

	module.switchBack = function () {
		if (module.displayAlternative() === true) {
			module.displayAlternative(false);
			module.alternativeAppShell(null);
		}
	};

	return module;
});