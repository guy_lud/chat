﻿using System.Web.Http;
using Ludvig.Modules.Contracts.Users;
using Ludvig.Web.UI.Infrastructure;

namespace Ludvig.Web.UI.Reaper.Users.Authentication
{
	[AllowAnonymousSessions]
	public class LoginController : ApiController
	{
		private readonly IUsersCreator _usersCreator;
		private readonly IUserWebAuthenticator _userWebAuthenticator;
		private readonly IUserRepository _userRepository;

		public LoginController(IUsersCreator usersCreator,
			IUserWebAuthenticator userWebAuthenticator,
			IUserRepository userRepository)
		{
			_usersCreator = usersCreator;
			_userWebAuthenticator = userWebAuthenticator;
			_userRepository = userRepository;
		}

		[Route("-/auth/login")]
		[HttpPost]
		public AuthenticationResult Login(LoginRequest request)
		{
			try
			{
				var user = _userRepository.GetUserByName(request.UserName);

				if (user == null)
				{
					_usersCreator.SignUp(request.UserName);
					user = _userRepository.GetUserByName(request.UserName);
				}

				_userWebAuthenticator.Login(request.UserName);

				return new AuthenticationResult();
			}
			catch (AuthenticationException)
			{
				return new AuthenticationResult("Something went wrong");
			}
		}
	}
}