﻿using Ludvig.Web.UI.Reaper.Spa;

namespace Ludvig.Web.UI.Reaper.Users.Authentication
{
	public class AjaxAuthenticationResult : AuthenticationResult
	{
		public AjaxAuthenticationResult(ChatClientModel spaClient)
		{
			SpaClient = spaClient;
		}

		public ChatClientModel SpaClient { get; }
	}
}