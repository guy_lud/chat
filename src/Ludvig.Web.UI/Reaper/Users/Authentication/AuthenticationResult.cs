﻿namespace Ludvig.Web.UI.Reaper.Users.Authentication
{
	public class AuthenticationResult
	{
		public AuthenticationResult()
		{
			IsSuccess = true;
		}

		public AuthenticationResult(string error)
		{
			Errors = new[] { error };
		}

		public string[] Errors { get; set; }
		public bool IsSuccess { get; set; }
	}
}