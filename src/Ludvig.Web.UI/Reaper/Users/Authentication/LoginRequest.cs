﻿namespace Ludvig.Web.UI.Reaper.Users.Authentication
{
	public class LoginRequest
	{
		public string UserName { get; set; }
	}
}