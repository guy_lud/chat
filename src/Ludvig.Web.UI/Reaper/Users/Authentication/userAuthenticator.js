define(['jquery'], function ($) {

	var module = {};

	module.authenticateUser = function (email, password) {

		var promise = $.ajax({
			url: '/auth/login',
			method: 'POST',
			data: JSON.stringify({ email: email, password: password }),
			contentType: 'application/json'
		});

		return promise;
	};

	return module;
});
