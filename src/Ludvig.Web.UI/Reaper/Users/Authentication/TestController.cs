﻿using System.Web.Http;
using Ludvig.Utilities.Mapping;

namespace Ludvig.Web.UI.Reaper.Users.Authentication
{
	public class TestController : ApiController
	{
		private readonly IObjectMapper _mapper;

		public TestController(IObjectMapper mapper)
		{
			_mapper = mapper;
		}
	}
}


//	public interface ITestSettings
//	{
//		[Default("test")]
//		string Name { get; set; }
//	}

//	[AllowAnonymousSessions]
//	public class TestController : Controller
//	{
//		private readonly IWelcomeEmailSender _welcomeEmailSender;
//		private readonly ILogger _logger;
//		private readonly ITestSettings _testSettings;

//		public TestController(IWelcomeEmailSender welcomeEmailSender,
//			ILogger logger,ITestSettings testSettings)
//		{
//			_welcomeEmailSender = welcomeEmailSender;
//			_logger = logger;
//			_testSettings = testSettings;
//		}

//		[Route("test")]

//		public async Task<string> Test(string email)
//		{
//			try
//			{
//				await _welcomeEmailSender.SendWelcomeEmailAsync(new User() { Email = email });
//			}
//			catch (Exception) 
//			{
//				throw;
//			}

//			return "Ok";
//		}

//		[System.Web.Mvc.Route("test1")]
//		public string Test1()
//		{
//			try
//			{
//				throw new ArgumentException("test one two");
//			}
//			catch (Exception e)
//			{
//				_logger.LogError("poky",e);
//			}


//			return "Ok";
//		}

//		[System.Web.Mvc.Route("test2")]
//		public string Test2()
//		{
//			return _testSettings.Name;
//		}


//	}


//	public class TestController2 : ApiController
//	{
//		[System.Web.Http.Route("guy"), System.Web.Http.HttpGet()]
//		public void Do(string name)
//		{

//		}
//	}

//	//public class Test2Controller : Controller
//	//{
//	//	private readonly IFoo _foo;

//	//	public Test2Controller(IFoo foo)
//	//	{
//	//		_foo = foo;
//	//	}

//	//	[Route("test3")]
//	//	public string Test2()
//	//	{
//	//		return "ok";
//	//	}
//	//}

//	//public interface IFoo
//	//{

//	//}

//	//class Foo : IFoo
//	//{
//	//	private readonly IArray[] _array;

//	//	public Foo(IArray[] array )
//	//	{
//	//		_array = array;
//	//	}
//	//}

//	//[RegisterAll]
//	//internal interface IArray
//	//{
//	//}

//	//class Array1 : IArray
//	//{
//	//}
//}