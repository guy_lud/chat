﻿using System.Web;
using Ludvig.Modules.Contracts.Users;

namespace Ludvig.Web.UI.Reaper.Users.Authentication
{
	public interface IUserWebAuthenticator
	{
		string Login(string userName);
	}

	public class UserWebAuthenticator : IUserWebAuthenticator
	{
		private readonly IUserAuthenticator _userAuthenticator;

		public UserWebAuthenticator(IUserAuthenticator userAuthenticator)
		{
			_userAuthenticator = userAuthenticator;
		}

		public string Login(string userName)
		{
			var sessionId = _userAuthenticator.Login(userName);
			HttpContext.Current.Response.Cookies.Add(new HttpCookie("sid", sessionId));

			return sessionId;
		}
	}
}