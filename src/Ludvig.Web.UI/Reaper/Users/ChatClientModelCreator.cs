using System.Linq;
using Ludvig.Modules.Contracts.Users;
using Ludvig.Utilities.Configuration;
using Ludvig.Web.UI.Reaper.Spa;

namespace Ludvig.Web.UI.Reaper.Users
{
	public class ChatClientModelCreator : IChatClientModelCreator
	{
		private readonly IHostingEnvironment _hostingEnvironment;
		

		public ChatClientModelCreator(IHostingEnvironment hostingEnvironment)
		{
			_hostingEnvironment = hostingEnvironment;
		}

		public ChatClientModel CreateModelForUser(User user)
		{
			return new ChatClientModel
			{
				Name = user?.UserName,
				Environment = _hostingEnvironment.EnvironmentName,
			};
		}
	}
}