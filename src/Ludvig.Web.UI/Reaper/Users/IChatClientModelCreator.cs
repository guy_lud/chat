﻿using Ludvig.Modules.Contracts.Users;
using Ludvig.Web.UI.Reaper.Spa;

namespace Ludvig.Web.UI.Reaper.Users
{
	public interface IChatClientModelCreator
	{
		ChatClientModel CreateModelForUser(User user);
	}
}