﻿using System.Web.Mvc;
using Ludvig.Modules.Contracts.Users;
using Ludvig.Web.UI.Infrastructure;

namespace Ludvig.Web.UI.Reaper.Hello
{
	[AllowAnonymousSessions]
	public class HelloController : Controller
	{
		private readonly ICurrentUserProvider _currentUserProvider;

		public HelloController(ICurrentUserProvider currentUserProvider)
		{
			_currentUserProvider = currentUserProvider;
		}

		[Route("hello")]
		public ActionResult Home()
		{
			if (_currentUserProvider.IsLoggedIn())
				return Redirect("/");

			return View("Hello/Hello");
		}
	}
}