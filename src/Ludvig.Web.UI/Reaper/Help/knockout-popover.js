﻿define(['knockout'], function(ko) {
    ko.bindingHandlers.popover = {
        init: function(element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = ko.utils.unwrapObservable(valueAccessor());
            var defaultOptions = {};
            options = $.extend(true, {}, defaultOptions, options);
            $(element).popover(options);
        }
    };
});