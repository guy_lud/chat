﻿define(function () {
	return {
		canActivate: function canActivate() {
			return { redirect: '!/mainboard' };
		}
	};
});