﻿require(['durandal/system',
	'durandal/app',
	'durandal/viewLocator',
	'url-builder'], function (system, app, viewLocator, urlBuilder) {
		//>>excludeStart("build", true);
		if (new urlBuilder().getQueryParam('debug') === 'true')
			system.debug(true);
		//>>excludeEnd("build");

		app.title = 'Chat';

		app.configurePlugins({
			router: true,
			dialog: true
		});

		app.start().then(function () {
			//Replace 'viewmodels' in the moduleId with 'views' to locate the view.
			//Look for partial views in a 'views' folder in the root.
			viewLocator.convertModuleIdToViewId = function (moduleId) { return moduleId + '-view'; };

			requirejs.config({ baseUrl: '/' });
			//Show the app by setting the root view model for our application with a transition.
			app.setRoot('shell');
		});
	});