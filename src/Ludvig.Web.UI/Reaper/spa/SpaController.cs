﻿using System.Web.Mvc;
using Ludvig.Modules.Contracts.Users;
using Ludvig.Web.UI.Reaper.Users;

namespace Ludvig.Web.UI.Reaper.Spa
{
	public class SpaController : Controller
	{
		private readonly IChatClientModelCreator _clientModelCreator;
		private readonly ICurrentUserProvider _currentUserProvider;

		public SpaController(IChatClientModelCreator clientModelCreator, ICurrentUserProvider currentUserProvider)
		{
			_clientModelCreator = clientModelCreator;
			_currentUserProvider = currentUserProvider;
		}

		[Route("")]
		public ActionResult Spa()
		{
			var model = new Spa
			{
				ClientModel = _clientModelCreator.CreateModelForUser(_currentUserProvider.GetCurrentUser())
			};

			return View("Spa/Spa", model);
		}
	}
}