﻿define([], function () {
	return function (id, isMe, userName ,message) {
		var self = this;
		self.id = id;
		self.direction = isMe ? 'right' : 'left';
		self.imageDirection = "pull-" + self.direction;
		self.image = isMe ? 'http://placehold.it/50/FA6F57/fff&text=ME' : 'http://placehold.it/50/55C1E7/fff&text=U';
		self.userName = userName;
		self.message = message;
	};
});