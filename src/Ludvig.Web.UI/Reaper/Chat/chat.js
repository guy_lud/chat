﻿define(['chat-line',
	'messages-handler',
	'knockout',
	'spaClientProvider'
], function (chatLine, handler, ko, spa) {

	var ctor = function () {

		var self = this;

		self.messages = ko.observableArray([]);
		self.textMessage = ko.observable('');
		self.disableSendButton = ko.observable(false);
		self.timer = null;

		fetchAllMessages();

		function initTimer() {
			if (self.timer != null)
				clearTimeout(self.timer);
			self.timer = setInterval(function () { fetchMessageDelta(); }, 5000);
		};

		function mapChatLines(messagesModel) {
			messagesModel.map(function (x) {
				self.messages.push(new chatLine(x.id, x.isMe, x.authorName, x.content));
			});
		};

		function fetchAllMessages() {
			var promise = handler.fetchAllMessagesPromise();

			promise.done(function (messagesModel) {

				mapChatLines(messagesModel);
				initTimer();
			})
				.fail(function () {
					alert('something went wrong, i\'ll fix this in production');
				});
		};

		function fetchMessageDelta() {
			var last = self.messages()[self.messages().length - 1];

			var promise = handler.fetchMessagesDeltaPromise(last.id);

			promise.done(function (messagesModel) {
				mapChatLines(messagesModel);
			}).fail(function () {
				
			});
		};

		self.postNewMassage = function () {

			var text = self.textMessage();

			if (self.textMessage() == '') {
				return;
			}

			self.disableSendButton(false);

			var promise = handler.postNewMessagePromise(text);

			promise.done(function (id) {
				var cl = new chatLine(id, true, spa.name, text);
				self.messages.push(cl);
			})
				.fail(function (error) {
					alert(error.responseText);
				})
				.always(function () {
					self.textMessage('');
					self.disableSendButton(false);
				});
		};
	};

	return ctor;
});