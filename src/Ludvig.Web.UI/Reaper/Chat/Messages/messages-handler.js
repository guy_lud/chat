﻿define(['jquery'], function ($) {

	var module = {};

	module.fetchAllMessagesPromise = function() {

		return $.ajax({
			url: '/message/fetch-all',
			method: 'GET',
			contentType: 'application/json'
		});
	};

	module.postNewMessagePromise = function (message) {

		var newMessageRequest =  {};

		newMessageRequest.text = message;

		return $.ajax({
			url: '/message/post-new-message',
			method: 'POST',
			data: JSON.stringify(newMessageRequest),
			contentType: 'application/json'
		});

	};

	module.fetchMessagesDeltaPromise = function(lastId) {

		var deltaMessageRequest = {};

		deltaMessageRequest.lastId = lastId;

		return $.ajax({
			url: '/message/fetch-delta-messages',
			method: 'POST',
			data: JSON.stringify(deltaMessageRequest),
			contentType: 'application/json'
		});
	};

	return module;
});