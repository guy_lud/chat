﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Ludvig.Modules.Contracts.Messages;
using Ludvig.Modules.Contracts.Users;

namespace Ludvig.Web.UI.Reaper.Chat.Messages
{
	public class MessagesController : ApiController
	{
		private readonly IMessageProvider _messageProvider;
		private readonly IMessageModelMapper _messageModelMapper;
		private readonly IMessageWriter _messageWriter;
		private readonly ICurrentUserProvider _currentUserProvider;


		public MessagesController(IMessageProvider messageProvider,
			IMessageModelMapper messageModelMapper,
			IMessageWriter messageWriter,
			ICurrentUserProvider currentUserProvider)
		{
			_messageProvider = messageProvider;
			_messageModelMapper = messageModelMapper;
			_messageWriter = messageWriter;
			_currentUserProvider = currentUserProvider;
		}

		[HttpGet]
		[Route("message/fetch-all")]
		public IEnumerable<MessageModel> FetchAllMessages()
		{
			var user = _currentUserProvider.GetCurrentUser();

			if (user == null)
				throw new UserDoesntExistsAuthenticationException();

			return _messageProvider.FetchAllMessages()
				.Select(x => _messageModelMapper.Map(x, user))
				.ToArray();
		}

		[HttpPost]
		[Route("message/post-new-message")]
		public int PostNewMessage(NewMessageRequest message)
		{
			var user = _currentUserProvider.GetCurrentUser();

			if (user == null)
				throw new UserDoesntExistsAuthenticationException();

			return _messageWriter.WriteMessgae(user.Id, message.Text);
		}

		[HttpPost]
		[Route("message/fetch-delta-messages")]
		public IEnumerable<MessageModel> FetchDeltaMessages(DeltaMessageRequest request)
		{
			var user = _currentUserProvider.GetCurrentUser();

			if (user == null)
				throw new UserDoesntExistsAuthenticationException();

			return _messageProvider.FetchDeltaMessages(request.LastId)
				.Select(x=> _messageModelMapper.Map(x, user));
		}
	}
}