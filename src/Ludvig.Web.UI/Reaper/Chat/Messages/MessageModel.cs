﻿namespace Ludvig.Web.UI.Reaper.Chat.Messages
{
	public class MessageModel
	{
		public int Id { get; set; }
		public bool IsMe { get; set; }
		public string Content { get; set; }
		public string AuthorName { get; set; }
	}
}