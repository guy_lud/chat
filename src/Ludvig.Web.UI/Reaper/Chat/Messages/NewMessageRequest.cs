﻿namespace Ludvig.Web.UI.Reaper.Chat.Messages
{
	public class NewMessageRequest
	{
		public string Text { get; set; }
	}
}