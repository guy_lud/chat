﻿using Ludvig.Modules.Contracts.Messages;
using Ludvig.Modules.Contracts.Users;

namespace Ludvig.Web.UI.Reaper.Chat.Messages
{
	public interface IMessageModelMapper
	{
		MessageModel Map(Message source, User currentUser);
	}


	class MessageModelMapper : IMessageModelMapper
	{
		private readonly IUserRepository _userRepository;

		public MessageModelMapper(IUserRepository userRepository)
		{
			_userRepository = userRepository;
		}

		public MessageModel Map(Message source, User currentUser)
		{
			// cache it out

			return new MessageModel
			{
				Id = source.Id,
				Content = source.Content,
				IsMe = currentUser.Id == source.SenderId,
				AuthorName = _userRepository.GetUserById(source.SenderId).UserName
			};
		}
	}
}