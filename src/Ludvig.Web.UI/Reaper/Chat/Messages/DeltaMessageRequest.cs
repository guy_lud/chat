﻿namespace Ludvig.Web.UI.Reaper.Chat.Messages
{
	public class DeltaMessageRequest
	{
		public int LastId { get; set; }
	}
}