﻿using System.Web.Http;
using Ludvig.Utilities.Logging;

namespace Ludvig.Web.UI.Reaper.System.Logging
{
	public class LogController : ApiController
	{
		[Route("-/system/1/log"), AllowAnonymous, HttpPost]
		public void LogClientMessage(LogEntry[] data)
		{
			// sometime i'll do that
		}
	}
}