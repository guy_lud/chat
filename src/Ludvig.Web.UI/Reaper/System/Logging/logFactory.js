﻿define(['log4javascript', 'spaClientProvider'], function (log, clientProvider) {

	var ajaxAppender = new log.AjaxAppender('/-/system/1/log');
	var isProduction = clientProvider.environment() === 'production';
	
	if (isProduction) {
		ajaxAppender.setThreshold(log.Level.ERROR);
	} else {
		ajaxAppender.setThreshold(log.Level.DEBUG);
	}

	var layout = new log.JsonLayout(!isProduction, isProduction);
	ajaxAppender.setLayout(layout);
	ajaxAppender.addHeader("Content-Type", "application/json; charset=utf-8");

	log.getRootLogger().addAppender(ajaxAppender);

	return log;
});