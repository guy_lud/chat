﻿define([], function() {
	return function() {
		var self = this;
		clearTimeout(window.errorReloaderTimer);
		self.is404 = document.location.hash === "#!/not-found";
	};
});