﻿define(['knockout', 'jquery', 'jquery-ui'], function(ko, $, ui) {
	ko.bindingHandlers.slider = {
		init: function(element, valueAccessor, allBindingsAccessor) {
			var options = allBindingsAccessor().sliderOptions || {};
			var observable = valueAccessor();

			if (observable().splice) {
				options.range = true;
			}

			options.slide = function(e, ui) {
				observable(ui.values ? ui.values : ui.value);
			};

			ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
				$(element).slider("destroy");
			});

			$(element).slider(options);
		},
		update: function(element, valueAccessor) {
			var value = ko.utils.unwrapObservable(valueAccessor());
			$(element).slider(value.slice ? "values" : "value", value);

		}
	};
});