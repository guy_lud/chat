﻿define(['knockout'], function (ko) {
	ko.bindingHandlers.affix = (function () {
		function setAffix(element) {
			setTimeout(function() {
                var el = $(element);
				var topOffset = el.offset().top + el.height();

                var placeholder = el.next();
			    placeholder.attr("class", placeholder.attr("class") + ' ' + el.attr("class"));
			    placeholder.removeClass('affix');
			    placeholder.height(el.height());

			    el.affix({ offset: { top: topOffset } });
			}, 200);
		}
		return { init: setAffix, update: setAffix };
	})();
});