﻿define(['jquery', 'global-loading', 'logFactory'], function ($, gl, logFactory) {

	var logger = logFactory.getLogger('global-logger');

	var model = {};

	function errorPage(msg, url, line, col, error) {

		if (line != null && line.ignoreErrors === true)
			return;

		var extra = !col ? '' : '\ncolumn: ' + col;
		extra += !error ? '' : '\nerror: ' + error;

		var message = "Error: " + msg + "\nurl: " + url + "\nline: " + line + extra;
		logger.error(message);

		alert(msg);
		console.log(msg);
		return;
	
	}

	model.configure = function () {
		$(document).ajaxStart(function (evt, xhr, settings) {
			console.log(evt); gl.isLoading(true); });
		$(document).ajaxStop(function () { gl.isLoading(false); });
		$(document).ajaxError(errorPage);
		window.onerror = errorPage;
	};
	return model;
});