﻿define(['knockout', 'jquery', 'jquery-ui'], function (ko, $, jqui) {
	ko.bindingHandlers.accordion = {
		init: function (element, valueAccessor, allBindings) {
			var el = $(element);
			el.data('isChangingPanel', false);

			var options = ko.unwrap(valueAccessor()) || {};

			var activePanel = 0;
			if (allBindings.has('activePanel')) {
				var activePanelVal = allBindings.get('activePanel');
				if (ko.isObservable(activePanelVal)) {
					el.data('activePanelObservable', activePanelVal);
					var innerVal = activePanelVal();
					if (innerVal == null)
						activePanelVal(0);
					else {
						activePanel = innerVal;
					}

					activePanelVal.subscribe(function () {
						if (el.data('isChangingPanel')) {
							el.data('isChangingPanel', false);
							return;
						}

						el.data('isChangingPanel', true);
						$(element).accordion('option', 'active', activePanelVal());
					});
				} else {
					activePanel = activePanelVal || 0;
				}
			}
			options.active = activePanel;


			setTimeout(function () {
				updateElementTimer(el, options);
			}, 0);

			//handle disposal (if KO removes by the template binding)
			ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
				el.accordion("destroy");
			});
		},
		update: function (element, valueAccessor) {
			var options = ko.unwrap(valueAccessor()) || {};
			updateElementTimer($(element), options);
			//        $('#accordion').accordion('option', 'active', ( $('#accordion').accordion('option','active') + delta  ));

		}
	};

	function updateElementTimer(el, options) {
		if (el.data('accordion-timer') != null) {
			clearTimeout(el.data('accordion-timer'));
		}

		el.data('accordion-timer', setTimeout(function () {
			updateElement(el, options);
		}, 100));
	}

	function updateElement(el, options) {
		if (typeof el.data("ui-accordion") != "undefined") {
			el.accordion("destroy");
		}
		el.accordion(options);

		el.on("accordionactivate", function (event, ui) {
			if (el.data('isChangingPanel')) {
				el.data('isChangingPanel', false);
				return;
			}

			el.data('isChangingPanel', true);
			var observable = el.data('activePanelObservable');
			if (observable)
				observable(el.accordion('option', 'active'));
		});
		
	}
});