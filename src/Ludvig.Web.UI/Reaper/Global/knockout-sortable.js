﻿define(['knockout'], function(ko) {
	return function(rows) {
		var self = this;
		self.rows = rows;

		self.currentKey = ko.observable();
		self.isAscending = ko.observable();

		self.sort = function(key) {
			self.isAscending(self.currentKey() === key && !self.isAscending());
			self.currentKey(key);

			self.rows.sort(function(a, b) {
				return a[key] > b[key] ? 1 : a[key] < b[key] ? -1 : 0;
			});

			if (!self.isAscending())
				self.rows.reverse();
		};

		self.iconClass = function(key) {
			if (self.currentKey() !== key)
				return 'fa-sort';

			if (self.isAscending())
				return 'fa-sort-asc';

			return 'fa-sort-desc';
		};
	};
});