﻿define(function () {
	var regex = /^(?!$)(?:(?:([A-z0-9]+?):(?=\/\/))?(?:\/\/)?([^:\/?]+)(?::(\d+))?)?(\/.*?(?=$|\?))?(?:(?!^)\?(.+))?$/;
	return function(url) {
		var self = this;
		url = url === undefined ? location.href : url;
		var matches = regex.exec(url);
		self.schema = matches[1];
		self.host = matches[2];
		self.port = matches[3];
		self.path = matches[4];

		var query = matches[5] || '';
		self.queryParams = query.split('&').map(function(x) {
			var parts = x.split('=');
			return { key: decodeURIComponent(parts[0]), value: decodeURIComponent(parts[1]) };
		}).filter(function (x) { return x.key != ''; });
		self.setQueryParam = function (key, value) {
			var existing = self.queryParams.filter(function (x) { return x.key === key; });
			if (existing.length > 0) {
				existing[0].value = value;
				return self;
			}

			self.queryParams.push({ key: key, value: value });
			return self;
		};
		self.removeQueryParam = function (key) {
			self.queryParams = self.queryParams.filter(function (x) { return x.key !== key; });
			return self;
		};
		self.getQueryParam = function (key) {
			var existing = self.queryParams.filter(function (x) { return x.key === key; });
			if (existing.length > 0) {
				return existing[0].value;
			}

			return undefined;
		};

		self.toString = function () {
			var str = '';
			if (self.schema)
				str = self.schema + '://';

			if (self.host)
				str += self.host;

			if (self.port)
				str += ':' + self.port;

			if (self.path)
				str += self.path;

			if (self.queryParams.length > 0) {
				str += '?';
				self.queryParams.forEach(function(x,i) {
					str += encodeURIComponent(x.key) + '=' + encodeURIComponent(x.value);
					if (i < self.queryParams.length - 1)
						str += '&';
				});
			}

			return str;
		};
	};
});