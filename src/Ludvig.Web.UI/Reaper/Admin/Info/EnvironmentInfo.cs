﻿using System.Collections.Generic;

namespace Ludvig.Web.UI.Reaper.Admin.Info
{
	public class EnvironmentInfo
	{
		public EnvironmentInfo()
		{
			Values = new Dictionary<string, string>();
		}

		public IDictionary<string,string> Values { get; set; }
	}
}