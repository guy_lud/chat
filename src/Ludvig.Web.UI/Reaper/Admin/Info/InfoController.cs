﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Web.Http;
using Ludvig.Utilities.Configuration;
using HostingEnvironment = System.Web.Hosting.HostingEnvironment;

namespace Ludvig.Web.UI.Reaper.Admin.Info
{
	public class InfoController : ApiController
	{
		private readonly IHostingEnvironment _hostingEnvironment;

		public InfoController(IHostingEnvironment hostingEnvironment)
		{
			_hostingEnvironment = hostingEnvironment;
		}

		[Route("op/info")]
		[HttpGet]
		public EnvironmentInfo Info()
		{
			var info = new EnvironmentInfo();
			info.Values.Add("Machine Name", Environment.MachineName);
			info.Values.Add("Machine Directory", Environment.CurrentDirectory);
			info.Values.Add("Machine User", Environment.UserName);
			info.Values.Add("Machine Process", Environment.CommandLine);
			info.Values.Add("Application Physical Path", HostingEnvironment.ApplicationPhysicalPath);
			info.Values.Add("Process Running Since", Process.GetCurrentProcess().StartTime.ToString(CultureInfo.InvariantCulture));
			info.Values.Add("Last Deployment Date", File.GetCreationTime(Assembly.GetExecutingAssembly().Location).ToString(CultureInfo.InvariantCulture));
			info.Values.Add("Hosting Environment", _hostingEnvironment.EnvironmentName);
			return info;
		}
	}
}