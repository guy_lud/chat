﻿using System.Web.Http;

namespace Ludvig.Web.UI.Reaper.Admin.Settings
{
	public class SettingsSanityController : ApiController
	{
		private readonly ISanitySettings _sanitySettings;

		public SettingsSanityController(ISanitySettings sanitySettings)
		{
			_sanitySettings = sanitySettings;
		}

		[Route("op/settings/sanity")]
		[HttpGet]
		public string Sanity()
		{
			return (_sanitySettings.IsDefaultValueSane && _sanitySettings.IsDbValueSane).ToString();
		}
	}
}