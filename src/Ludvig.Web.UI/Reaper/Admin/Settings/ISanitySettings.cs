using Ludvig.Utilities.Settings;

namespace Ludvig.Web.UI.Reaper.Admin.Settings
{
	public interface ISanitySettings
	{
		[Default(true)]
		bool IsDefaultValueSane { get; set; }

		[Default(false)]
		bool IsDbValueSane { get; set; }
	}
}