using Ludvig.Infrastructure.Bootstrap;

namespace Ludvig.Web.UI.Infrastructure.Configuration
{
	public static class WebBootstrapperModulesExtensions
	{
		public static ModuleCollection AddMvc(this ModuleCollection target)
		{
			target.Add(new MvcBootstrapperModule());
			return target;
		}

		public static IApplicationBuilder UseMvc(this IApplicationBuilder target)
		{
			target.AddModule(new MvcBootstrapperModule());
			return target;
		}

		public static ModuleCollection AddWebApi(this ModuleCollection target)
		{
			target.Add(new WebApiBootstrapperModule());
			return target;
		}

		public static IApplicationBuilder UseWebApi(this IApplicationBuilder target)
		{
			target.AddModule(new WebApiBootstrapperModule());
			return target;
		}
	}
}