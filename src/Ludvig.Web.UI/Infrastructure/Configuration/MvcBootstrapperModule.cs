using System.Web.Mvc;
using System.Web.Routing;
using Ludvig.Infrastructure.Bootstrap;
using Ludvig.Web.UI.Filters;
using Ludvig.Web.UI.Infrastructure.Rendering;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;

namespace Ludvig.Web.UI.Infrastructure.Configuration
{
	public class MvcBootstrapperModule : IBootstrapModule , IAppBuilderModule
	{
		public void OnBootstrap(BootsrapperContext context)
		{
			context.Container.RegisterPerWebRequest<ILayoutContext, RepearLayoutContex>();

			context.Container.RegisterMvcControllers(typeof(MvcApplication).Assembly);

			context.Container.RegisterMvcIntegratedFilterProvider();

			DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(context.Container));
		}

		public void OnBuild(AppBuilderContext context)
		{
			var resolver = (SimpleInjectorServiceResolver)context.Resolver;

			DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(resolver.Container));

			EnginesViewBootstraper.Bootstrap(ViewEngines.Engines);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);

			var filters = resolver.ResolveAll<IGlobalFilter>();
			foreach (IGlobalFilter filter in filters)
			{
				GlobalFilters.Filters.Add(filter);
			}
		}
	}
}
