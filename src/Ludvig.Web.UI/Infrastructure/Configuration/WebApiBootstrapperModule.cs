using System.Web.Http;
using Ludvig.Infrastructure.Bootstrap;
using Ludvig.Web.UI.Filters;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;

namespace Ludvig.Web.UI.Infrastructure.Configuration
{
	public class WebApiBootstrapperModule : IBootstrapModule , IAppBuilderModule
	{
		public void OnBootstrap(BootsrapperContext context)
		{
			context.Container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
		}

		public void OnBuild(AppBuilderContext context)
		{
			var resolver = (SimpleInjectorServiceResolver)context.Resolver;

			GlobalConfiguration.Configure(WebApiConfig.Register);

			var filters = resolver.ResolveAll<IGlobalFilter>();
			foreach (IGlobalFilter filter in filters)
			{
				GlobalConfiguration.Configuration.Filters.Add(filter);
			}

			GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(resolver.Container);
		}
	}
}