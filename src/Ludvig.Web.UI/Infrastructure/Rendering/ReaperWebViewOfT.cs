﻿using System.Web.Mvc;
using Ludvig.Utilities.Ioc;
using Ludvig.Web.UI.Infrastructure.Bunduru;

namespace Ludvig.Web.UI.Infrastructure.Rendering
{
	public abstract class ReaperWebView<T> : WebViewPage<T>
	{
		protected ILayoutContext LayoutContext  => GlobalServiceResolver.Resolve<ILayoutContext>();
		protected IBunduruHashProvider Bunduru => GlobalServiceResolver.Resolve<IBunduruHashProvider>();
	}
}