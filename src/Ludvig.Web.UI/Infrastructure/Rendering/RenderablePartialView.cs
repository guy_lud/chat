﻿using System.Web;
using System.Web.Mvc.Html;

namespace Ludvig.Web.UI.Infrastructure.Rendering
{
	public class RenderablePartialView : IRenderable
	{
		public virtual string Path { get; set; }

		public IHtmlString Render(IRenderingContex contex)
		{
			return contex.HtmlHelper.Partial(Path, this);
		}
	}
}