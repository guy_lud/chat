﻿using System.Web.Mvc;

namespace Ludvig.Web.UI.Infrastructure.Rendering
{
	public class ReaperViewEngine : RazorViewEngine
	{
		public ReaperViewEngine()
			: this(null)
		{ }

		public ReaperViewEngine(IViewPageActivator viewPageActivator)
			: base(viewPageActivator)
		{
			AreaMasterLocationFormats = new[] { "~/Reaper/{0}.cshtml" };
			AreaViewLocationFormats = new[] { "~/Reaper/{0}.cshtml" };
			AreaPartialViewLocationFormats = new[] { "~/Reaper/{0}.cshtml" };
			ViewLocationFormats = new[] { "~/Reaper/{0}.cshtml" };
			MasterLocationFormats = new[] { "~/Reaper/{0}.cshtml" };
			PartialViewLocationFormats = new[] { "~/Reaper/{0}.cshtml" };
			FileExtensions = new[] { "cshtml" };
		}
	}
}