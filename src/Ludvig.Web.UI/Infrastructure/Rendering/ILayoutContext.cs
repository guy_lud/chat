﻿namespace Ludvig.Web.UI.Infrastructure.Rendering
{
	public interface ILayoutContext
	{
		string Title { get; set; }
	}

	public class RepearLayoutContex : ILayoutContext
	{
		public string Title { get; set; }
	}
}