using System.Web;

namespace Ludvig.Web.UI.Infrastructure.Rendering
{
	public interface IRenderable
	{
		IHtmlString Render(IRenderingContex contex);
	}
}