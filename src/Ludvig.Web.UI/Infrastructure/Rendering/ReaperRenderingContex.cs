using System.Web.Mvc;

namespace Ludvig.Web.UI.Infrastructure.Rendering
{
	public class ReaperRenderingContex : IRenderingContex
	{
		public ReaperRenderingContex(HtmlHelper htmlHelper)
		{
			HtmlHelper = htmlHelper;
		}

		public HtmlHelper HtmlHelper { get; }
	}
}