﻿using System;

namespace Ludvig.Web.UI.Infrastructure.Rendering
{
	public class ViewComponentException : Exception
	{
		public ViewComponentException(Type type, Exception inner)
			: base("Error in ViewComponent " + type, inner)
		{
			ViewComponentType = type;
		}

		public Type ViewComponentType { get; set; }
	}
}