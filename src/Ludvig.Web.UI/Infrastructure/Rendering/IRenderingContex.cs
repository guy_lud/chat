using System.Web.Mvc;

namespace Ludvig.Web.UI.Infrastructure.Rendering
{
	public interface IRenderingContex
	{
		HtmlHelper HtmlHelper { get; }
	}
}