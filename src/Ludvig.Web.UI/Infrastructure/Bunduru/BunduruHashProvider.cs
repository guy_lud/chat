﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Ludvig.Utilities.Caching;
using Ludvig.Utilities.Collections;
using Ludvig.Utilities.Crypto;
using Ludvig.Utilities.Formatting;

namespace Ludvig.Web.UI.Infrastructure.Bunduru
{
	public interface IBunduruHashProvider
	{
		string GetPath(string bunduru, BunduruType bunduruType);
	}

	//Need to refactor, I know, but first we need to see where this is going exactly 
	public class BunduruHashProvider : IBunduruHashProvider
	{
		private const string CachePrefix = "Bunduru_";
		private static readonly object LockMeLikeOneOfYourFrenchGirls = new object();

		private readonly HashSet<string> _exemptionFolders = new HashSet<string> { "hello", "lib" }; //config file?

		private readonly IWebApplicationContextProvider _webApplicationContextProvider;
		private readonly IInMemoryCacher _inMemoryCacher;
		private readonly IMurmurHasher _murmurHasher;
		private readonly IBunduruHandler[] _bunduruHandlers;
		private readonly IBunduruSettings _bunduruSettings;

		public BunduruHashProvider(IWebApplicationContextProvider webApplicationContextProvider,
			IInMemoryCacher inMemoryCacher,
			IMurmurHasher murmurHasher,
			IEnumerable<IBunduruHandler> bunduruHandlers, IBunduruSettings bunduruSettings)
		{
			_webApplicationContextProvider = webApplicationContextProvider;
			_inMemoryCacher = inMemoryCacher;
			_murmurHasher = murmurHasher;
			_bunduruSettings = bunduruSettings;
			_bunduruHandlers = bunduruHandlers.ToArray();
		}

		public string GetPath(string bunduru, BunduruType bunduruType)
		{
			var cachedPath = _inMemoryCacher.Get<string>(GetCacheKey(bunduru, bunduruType));
			if (!_bunduruSettings.IsDev && cachedPath != null)
				return cachedPath;

			lock (LockMeLikeOneOfYourFrenchGirls)
			{
				return GetPathInLockedContext(bunduru, bunduruType);
			}
		}

		private string GetPathInLockedContext(string groupName, BunduruType bunduruType)
		{
			//double check, because that's how we do locks
			var cachedPath = _inMemoryCacher.Get<string>(GetCacheKey(groupName, bunduruType));
			if (!_bunduruSettings.IsDev && cachedPath != null)
				return cachedPath;

			var files = GetFiles(groupName, bunduruType)
				.Where(x => IsHandleable(x.Name))
				.OrderByDescending(x => x.Name == "almond-custom.js") //config file?
				.ThenByDescending(x => x.Name == "jquery-1.9.1.js")
				.ThenByDescending(x => x.Name.StartsWith("_"))
				.ThenBy(x => x.Name)
				.ToList();

			if (!files.Any())
				return "{0}.{1}.no-files.{2}".With(groupName, bunduruType.ToString().ToLower(),
					bunduruType.GetOutputBunduruFileExtension());

			var isBunduruModified = false;
			var isImportScssModified = false;
			var handler = _bunduruHandlers.First(x => x.HandledType == bunduruType);
			foreach (var file in files)
			{
				var lastModified = _inMemoryCacher.Get<long>(GetFileCacheKey(file.FullName));
				var fileTime = file.LastWriteTime.ToFileTime();
				if (lastModified == fileTime && !isImportScssModified)
					continue;

				_inMemoryCacher.Save(GetFileCacheKey(file.FullName), fileTime);
				isBunduruModified = true;
				if (bunduruType == BunduruType.Css && file.Name.StartsWith("_"))
					isImportScssModified = true;

				var fileVirtualPath = (file.DirectoryName ?? "")
					.Replace(_webApplicationContextProvider.GetPhysicalPath(), "")
					.TrimStart('\\')
					.Replace(@"reaper\", "")
					.Replace('\\', '/')
					//we want the module name to be case-sensitive while the path always lower-case,
					//i.e. my-feature/sub-context/myFeature
					.ToLower() + '/' + file.Name;
				var bunduruFile = new BunduruFile
				{
					Content = File.ReadAllText(file.FullName),
					VirtualPath = fileVirtualPath,
					PhysicalPath = file.FullName
				};
				var handledContent = handler.GetHandledContent(bunduruFile);
				File.WriteAllText(handler.GetOutputFileName(file.FullName), handledContent);
			}

			if (!isBunduruModified && cachedPath != null)
			{
				return cachedPath;
			}

			var stringBuilder = new StringBuilder();
			foreach (var file in files)
			{
				var handledName = handler.GetOutputFileName(file.FullName);
				if (_bunduruSettings.IsDev)
					stringBuilder.AppendLine("/* {0} */".With(file.FullName));

				stringBuilder.Append(File.ReadAllText(handledName));

				if (_bunduruSettings.IsDev)
					stringBuilder.AppendLine();
			}
			
			var hash = _murmurHasher.GetHash(stringBuilder.ToString());
			var physicalPath = _webApplicationContextProvider.GetPhysicalPath() + @"\bunduru\";
			var dir = Directory.CreateDirectory(physicalPath);
			var fileName = FormatBunduruFileName(groupName, bunduruType, hash);
			var virtualPath = "/bunduru/" + fileName;
			_inMemoryCacher.Save(GetCacheKey(groupName, bunduruType), virtualPath);

			if (File.Exists(fileName))
				return virtualPath;

			//remove older files
			dir.GetFiles(FormatBunduruFileName(groupName, bunduruType, "*")).ForEach(x => x.Delete());

			File.WriteAllText(physicalPath + fileName, stringBuilder.ToString());

			return virtualPath;
		}

		private string FormatBunduruFileName(string group, BunduruType type, string hash)
		{
			return "{0}{1}.{2}.{3}".With(group, type == BunduruType.Html ? "-templates" : null, hash,
				type.GetOutputBunduruFileExtension());
		}

		private bool IsHandleable(string name)
		{
			name = name.Remove(name.LastIndexOf('.'));
			return new[] {"min", "intellisense", "debug", "bunduru-min"}.All(suffix => !name.EndsWith("." + suffix));
		}

		private string GetFileCacheKey(string fileName)
		{
			fileName = fileName.Replace(_webApplicationContextProvider.GetPhysicalPath(), "");
			return CachePrefix + "File_" + fileName;
		}

		private IList<FileInfo> GetFiles(string bunduru, BunduruType bunduruType)
		{
			var reaper = new DirectoryInfo(_webApplicationContextProvider.GetPhysicalPath() + @"\reaper");
			var dirs = bunduru == "all"
				? reaper.GetDirectories().Where(x => !x.Name.ToLower().In(_exemptionFolders)).ToList()
				: new List<DirectoryInfo> {new DirectoryInfo(reaper.FullName + @"\" + bunduru)};

			return dirs.SelectMany(x => x.GetFiles("*.{0}".With(bunduruType.GetOriginalFileExtension()), SearchOption.AllDirectories)).ToList();
		}

		private string GetCacheKey(string bunduru, BunduruType bunduruType)
		{
			return "{0}_{1}".With(bunduru, bunduruType);
		}
	}

	public static class BunduruHashProviderExtensions
	{
		private const string ScriptTag = "<script src=\"{0}\"></script>";
		private const string StylesheetTag = "<link rel=\"stylesheet\" href=\"{0}\" />";

		public static HtmlString GetScriptTag(this IBunduruHashProvider target, string bunduru)
		{
			return new HtmlString(ScriptTag.With(target.GetPath(bunduru, BunduruType.Js)));
		}

		public static HtmlString GetHtmlTemplates(this IBunduruHashProvider target, string bunduru)
		{
			return new HtmlString(ScriptTag.With(target.GetPath(bunduru, BunduruType.Html)));
		}

		public static HtmlString GetStyleSheetTag(this IBunduruHashProvider target, string bunduru)
		{
			return new HtmlString(StylesheetTag.With(target.GetPath(bunduru, BunduruType.Css)));
		}

		public static HtmlString GetAllStyleSheetTag(this IBunduruHashProvider target)
		{
			return new HtmlString(StylesheetTag.With(target.GetPath("all", BunduruType.Css)));
		}
	}
}