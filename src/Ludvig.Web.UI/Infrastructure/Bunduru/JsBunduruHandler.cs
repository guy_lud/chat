﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Ludvig.Utilities.Formatting;
using Microsoft.Ajax.Utilities;

namespace Ludvig.Web.UI.Infrastructure.Bunduru
{
	public class JsBunduruHandler : IBunduruHandler
	{
		private readonly Minifier _minifier;
		private readonly IBunduruSettings _bunduruSettings;

		public JsBunduruHandler(IBunduruSettings bunduruSettings)
		{
			_bunduruSettings = bunduruSettings;
			_minifier = new Minifier(); //DI
		}

		public BunduruType HandledType => BunduruType.Js;

		public string GetHandledContent(BunduruFile file)
		{
			var content = HandleDefine(file.Content, file.VirtualPath);
			if (_bunduruSettings.IsDev)
				return content;

			var settings = new CodeSettings
			{
				PreserveImportantComments = false,
				OutputMode = OutputMode.SingleLine,
				TermSemicolons = true
			};
			return _minifier.MinifyJavaScript(content, settings);
		}

		private string HandleDefine(string content, string relativePath)
		{
			var moduleId = relativePath.Replace(".js", "");
			var shortModuleId = ShortenModuleId(moduleId);
			moduleId = shortModuleId == moduleId ? moduleId.Split('/').Last() : shortModuleId;

			return Regex.Replace(content, @"(?<=^|\s|\r|\n|\?|\{)define\(\s*(?=\[|function|factory)", "define('{0}', ".With(moduleId), RegexOptions.Multiline);
		}

		public string GetOutputFileName(string fullFileName)
		{
			if (!fullFileName.EndsWith(".js"))
				throw new InvalidOperationException();

			return fullFileName.Remove(fullFileName.Length - 3) + ".bunduru-min.js";
		}

		public string ShortenModuleId(string moduleId)
		{
			//config file
			return moduleId.Replace("lib/require/text", "text")
						.Replace("lib/durandal/js/plugins", "plugins")
						.Replace("lib/durandal/js/transitions", "transitions")
						.Replace("lib/durandal/js", "durandal")
						.Replace("lib/knockout/knockout.mapping", "knockout-mapping")
						.Replace("lib/knockout/knockout", "knockout")
						.Replace("lib/jquery/jquery-1.9.1", "jquery")
						.Replace("lib/jquery-ui/jquery-ui", "jquery-ui");
		}
	}
}