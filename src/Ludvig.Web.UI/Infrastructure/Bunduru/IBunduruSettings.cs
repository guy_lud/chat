﻿using Ludvig.Utilities.Settings;

namespace Ludvig.Web.UI.Infrastructure.Bunduru
{
	public interface IBunduruSettings
	{
		[Default(true)]
		bool IsDev { get; set; } 
	}
}