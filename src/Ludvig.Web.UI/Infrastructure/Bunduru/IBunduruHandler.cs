using Ludvig.Utilities.Ioc;

namespace Ludvig.Web.UI.Infrastructure.Bunduru
{
	[RegisterAll]
	public interface IBunduruHandler
	{
		BunduruType HandledType { get; }
		string GetHandledContent(BunduruFile file);
		string GetOutputFileName(string fullFileName);
	}
}