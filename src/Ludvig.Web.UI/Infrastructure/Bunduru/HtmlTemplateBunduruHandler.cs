﻿using System;
using System.Linq;
using System.Web;
using Ludvig.Utilities.Formatting;

namespace Ludvig.Web.UI.Infrastructure.Bunduru
{
	public class HtmlTemplateBunduruHandler : IBunduruHandler
	{
		private const string HtmlTemplateDefinition = @"define('text!{0}-view.html', function() {{ return '{1}'; }});";

		public BunduruType HandledType => BunduruType.Html;

		public string GetHandledContent(BunduruFile file)
		{
			var moduleId = file.VirtualPath.Split('/').Last().Replace(".html", "");
			var escapedHtml = HttpUtility.JavaScriptStringEncode(file.Content);
			return HtmlTemplateDefinition.With(moduleId, escapedHtml);
		}

		public string GetOutputFileName(string fullFileName)
		{
			if (!fullFileName.EndsWith(".html"))
				throw new InvalidOperationException();

			return fullFileName.Remove(fullFileName.Length - 4) + "template.bunduru-min.js";
		}
	}
}