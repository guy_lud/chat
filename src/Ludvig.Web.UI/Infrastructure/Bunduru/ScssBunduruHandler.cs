﻿using System;
using LibSassHost;
using Microsoft.Ajax.Utilities;

namespace Ludvig.Web.UI.Infrastructure.Bunduru
{
	public class ScssBunduruHandler : IBunduruHandler
	{
		private readonly IBunduruSettings _bunduruSettings;
		private readonly SassCompiler _sassCompiler;
		private readonly Minifier _minifier;

		public ScssBunduruHandler(IBunduruSettings bunduruSettings)
		{
			_bunduruSettings = bunduruSettings;
			_sassCompiler = new SassCompiler();
			_minifier = new Minifier();
		}
		public BunduruType HandledType => BunduruType.Css;

		public string GetHandledContent(BunduruFile file)
		{
			var compiledContent = _sassCompiler.Compile(file.Content, file.PhysicalPath).CompiledContent;
			if (_bunduruSettings.IsDev)
				return compiledContent;

			var minifiedSheet = _minifier.MinifyStyleSheet(compiledContent, new CssSettings { CommentMode = CssComment.None });
			return minifiedSheet;
		}

		public string GetOutputFileName(string fullFileName)
		{
			if (!fullFileName.EndsWith(".scss"))
				throw new InvalidOperationException();

			return fullFileName.Remove(fullFileName.Length - 5) + ".bunduru-min.css";
		}
	}
}