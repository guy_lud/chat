namespace Ludvig.Web.UI.Infrastructure.Bunduru
{
	//Use object instead of enum
	public enum BunduruType
	{
		Html,
		Js,
		Css
	}

	public static class BunduruTypeExtensions
	{
		public static string GetOriginalFileExtension(this BunduruType bunduruType)
		{
			return bunduruType == BunduruType.Css ? "scss" : bunduruType.ToString().ToLower();
		}

		public static string GetOutputBunduruFileExtension(this BunduruType bunduruType)
		{
			return bunduruType == BunduruType.Html ? "js" : bunduruType.ToString().ToLower();
		}
	}
}