﻿namespace Ludvig.Web.UI.Infrastructure.Bunduru
{
	public class BunduruFile
	{
		public string Content { get; set; } 
		public string VirtualPath { get; set; } 
		public string PhysicalPath { get; set; } 
	}
}