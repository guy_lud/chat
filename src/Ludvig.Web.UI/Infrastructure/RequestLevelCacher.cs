﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ludvig.Utilities.Caching;

namespace Ludvig.Web.UI.Infrastructure
{
	public class RequestLevelCacher : IRequestLevelCacher
	{
		public ICollection<T> Get<T>(IEnumerable<string> keys)
		{
			return keys
				.Where(x => Bag.Contains(x))
				.Select(x => Bag[x])
				.Cast<T>()
				.ToList();
		}

		public void Save<T>(string key, T item, TimeSpan? cacheDuration = null)
		{
			if (cacheDuration.HasValue)
				throw new InvalidOperationException("Request-level cache doesn't allow expiration time");

			Bag[key] = item;
		}

		public void Invalidate(IEnumerable<string> keys)
		{
			foreach (var key in keys)
			{
				Bag.Remove(key);
			}
		}

		public void InvalidateAll()
		{
			Bag.Clear();
		}

		private IDictionary Bag => HttpContext.Current.Items;
	}
}