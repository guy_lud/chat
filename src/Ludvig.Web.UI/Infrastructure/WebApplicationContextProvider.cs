﻿using System.Web;
using System.Web.Hosting;
using Ludvig.Utilities.Formatting.UrlHandling;

namespace Ludvig.Web.UI.Infrastructure
{
	public interface IWebApplicationContextProvider
	{
		string GetPhysicalPath();
		HttpContext GetHttpContext();
	}

	public class WebApplicationContextProvider : IWebApplicationContextProvider
	{
		public string GetPhysicalPath()
		{
			return HostingEnvironment.ApplicationPhysicalPath;
		}

		public HttpContext GetHttpContext()
		{
			return HttpContext.Current;
		}
	}

	public static class WebApplicationContextProviderExtensions
	{
		public static bool IsLocal(this IWebApplicationContextProvider target)
		{
			return target.GetHttpContext().Request.IsLocal;
		}

		public static bool HasQueryParameter(this IWebApplicationContextProvider target, string parameter)
		{
			if (parameter == null)
				return false;

			var url = target.GetHttpContext()
				.Request
				.Url.ToString();

			var builder= new UrlBuilder(url);

			return builder.Query.Values.Contains(parameter);
		}
	}
}