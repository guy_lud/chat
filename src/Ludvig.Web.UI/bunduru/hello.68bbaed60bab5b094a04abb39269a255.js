/* C:\dev\tempchat\src\Ludvig.Web.UI\reaper\hello\hello.js */
$(function () {
	var authenticationForm = $('.form-signin');

	var userNameField = $('#user-name');

	var signInButton = $('#sign-in-btn');

	var errors = $(".errors");

	signInButton.click(function () {
		var userName = userNameField.val();

		if (!isValidUserName(userName)) {
			setError('user name must be valid');
			return false;
		}

		var loginRequest = { userName: userName };

		startLoading();
		$.ajax({
			url: '-/auth/login',
			method: 'POST',
			data: JSON.stringify(loginRequest),
			contentType: 'application/json',
			success: authenticationSuccess,
			error: authenticationError
		});
	});

	function authenticationSuccess(response) {
		if (response.isSuccess) {
			location.href = "/";
			return;
		}

		setError(response.errors);
		stopLoading();
	}

	function isValidUserName(userName) {
		if (!userName || userName == "")
			return false;

		return true;
	}

	function authenticationError() {
		setError("An unknown error has occurred. Please try again later.");
		stopLoading();
	}

	function setError(text) {
		errors.text(text);
	}

	function startLoading() {
		authenticationForm.attr('disabled', true);
	}

	function stopLoading() {
		authenticationForm.attr('disabled', false);
	}
});

