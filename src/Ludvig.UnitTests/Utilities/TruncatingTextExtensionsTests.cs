﻿using Hermes.Utilities.Formatting;
using NUnit.Framework;

namespace Hermes.UnitTests.Utilities
{
	[TestFixture]
    public class TruncatingTextExtensionsTests
	{
		[Test]
		[TestCase("Fish", ExpectedResult = "Fish")]
		[TestCase("Fish and Chips", ExpectedResult = "Fi...")]
		public string TruncateTestCases(string text)
		{
			return text.Truncate(5);
		}

		[Test]
		[TestCase("Fish", ExpectedResult = "Fish")]
		[TestCase("Fish and Chips", ExpectedResult = "Fish and...")]
		[TestCase("His name was king", ExpectedResult = "His name...")]
		public string TruncateWordsTestCases(string text)
		{
			return text.TruncateWords(12);
		}
	}
}
