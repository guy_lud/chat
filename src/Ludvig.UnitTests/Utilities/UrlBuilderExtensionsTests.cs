using Hermes.Utilities.Formatting.UrlHandling;
using NUnit.Framework;

namespace Hermes.UnitTests.Utilities
{
	[TestFixture]
	public class UrlBuilderExtensionsTests
	{
		[Test]
		[TestCase("/?lol=1&lmao=2", "/?rofl=false", Result = "/?lol=1&lmao=2&rofl=false")]
		[TestCase("www.goo.com/?lol=1&lmao=2", "/?ma=or", Result = "//www.goo.com/?lol=1&lmao=2&ma=or")]
		[TestCase("www.goo.com", "/?sid=tambur", Result = "//www.goo.com?sid=tambur")]
		[TestCase("www.goo.com", null, Result = "//www.goo.com")]
		[TestCase("https://www.bas.com", "http://yes/no?what=ok", Result = "https://www.bas.com?what=ok")]
		public string WithQueryParamsFromAnotherUrl_UrlWithQuery_Added(string originalUrl, string targetUrl)
		{
			var result = originalUrl.ToUrlBuilder()
				.WithQueryParamsFromAnotherUrl(targetUrl)
				.ToString();

			return result;
		}
	}
}