﻿using System;
using System.Linq;
using FluentAssertions;
using Hermes.Utilities.Formatting;
using Hermes.Utilities.Formatting.UrlHandling;
using NUnit.Framework;

namespace Hermes.UnitTests.Utilities
{
	[TestFixture]
	public class UrlBuilderTests
	{
		[Test]
		public void UrlBuilder_WhenCasualUrl_PartsAreCorrect()
		{
			const string url = "http://www.shopyourway.com";
			var builder = new UrlBuilder(url);
			builder.Protocol.ShouldBeEquivalentTo("http");
			builder.Host.ShouldBeEquivalentTo("www.shopyourway.com");
			builder.Path.Should().BeNull();
			builder.ToString().ShouldBeEquivalentTo(url);
		}

		[Test]
		public void UrlBuilder_WhenUrlWithQuery_QueryIsGood()
		{
			const string url = "https://youtube/page.php?color=red&width=200&height=300";
			var builder = new UrlBuilder(url);
			builder.Query.GetValue("color").ShouldBeEquivalentTo("red");
			builder.Query.GetValue("width").ShouldBeEquivalentTo("200");
			builder.Query.GetValue("height").ShouldBeEquivalentTo("300");
			builder.Query.Values.Should().HaveCount(3);
		}

		[Test]
		[TestCase("http://www.biatch.com", Result = "www.biatch.com")]
		[TestCase("http://www.biatch.com", Result = "www.biatch.com")]
		[TestCase("http://www.biatch.com:1010", Result = "www.biatch.com")]
		[TestCase("http://kewl:1787010/hhuhu", Result = "kewl")]
		[TestCase("kewl:1787010/hhuhu", Result = "kewl")]
		[TestCase("baaa/hhuhu", Result = "baaa")]
		[TestCase("/hhuhu", Result = null)]
		public string UrlBuilder_WhenVariousHosts_ParsedCorrectly(string url)
		{
			return new UrlBuilder(url).Host;
		}

		[Test]
		public void UrlBuilder_RelativeUrl_PathExists()
		{
			const string url = "/whatsup/allgoodya?jey=sa&do=ba";
			var builder = new UrlBuilder(url);
			builder.Protocol.Should().BeNull();
			builder.Host.Should().BeNull();
			builder.Port.HasValue.Should().BeFalse();
			builder.Path.ShouldBeEquivalentTo("/whatsup/allgoodya");
			builder.Query.Values.Should().HaveCount(2);
			builder.Query.GetValue("jey").ShouldBeEquivalentTo("sa");
			builder.Query.GetValue("do").ShouldBeEquivalentTo("ba");
		}

		[Test]
		[TestCase("hello")]
		[TestCase("hello man")]
		[TestCase("hello man, how you doing")]
		[TestCase("hello man, how you doing?")]
		[TestCase("hello man, how you doin'?")]
		[TestCase("&hello &=?man=, how!@ you doin'?")]
		public void UrlBuilder_QueryParameters_ShouldBeEncodedAndDecodedCorrectly(string param)
		{
			var builder = new UrlBuilder("www.jinx.com");
			builder.Query.AddOrUpdate("k", param);
			var secondBuilder = new UrlBuilder(builder.ToString());
			secondBuilder.Query.GetValue("k").ShouldBeEquivalentTo(param);
		}

		[Test]
		public void UrlBuilder_QueryParameters_ShouldFormatArraysIntoMultipleKeys()
		{
			var builder = new UrlBuilder();
			builder.Query.AddOrUpdateCollection("ids", new[] {"22", "#33", "444"});
			var url = builder.ToString();
			var key = Uri.EscapeDataString("ids[]");
			var secondValue = Uri.EscapeDataString("#33");
			url.ShouldBeEquivalentTo("?{0}=22&{0}={1}&{0}=444".With(key, secondValue));
		}

		[Test]
		public void UrlBuilder_QueryParameters_ShouldParseArrays()
		{
			var key = Uri.EscapeDataString("ids[]");
			var url = $"http://www.dragons.com/?{key}=1&{key}=3&{key}=hello";
			var builder = new UrlBuilder(url);
			var values = builder.Query.GetValues("ids").ToArray();
			values.Length.ShouldBeEquivalentTo(3);
			values[0].ShouldBeEquivalentTo("1");
			values[1].ShouldBeEquivalentTo("3");
			values[2].ShouldBeEquivalentTo("hello");
			builder.ToString().ShouldBeEquivalentTo(url);
		}

		[Test]
		public void UrlBuilder_QueryParameters_EmptyParam()
		{
			var url = "/index.html?a=1&b";
			var builder = new UrlBuilder(url);
			builder.Query.GetValue("b").ShouldBeEquivalentTo(null);
			builder.ToString().ShouldBeEquivalentTo(url);
		}
	}
}