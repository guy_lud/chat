﻿using System.Collections.Generic;
using System.Web;
using Hermes.Utilities.Formatting;
using UAParser;

namespace Hermes.Modules.Tracker
{
	public class RequestParser
	{
		private static Parser _parser;

		static RequestParser()
		{
			_parser = Parser.GetDefault();
		}

		public static Dictionary<string, object> ExtractRequestParameters(HttpRequest request)
		{
			var browser = request.Browser;
			var queryDict = new Dictionary<string, string>();
			// Have to iterate through the list this way, because query tokens without values ("&word") 
			// get interpreted as a value with key == null, which throws a null reference exception
			for (int i = 0; i < request.QueryString.Count; i++)
			{
				var key = request.QueryString.GetKey(i);
				var value = request.QueryString.Get(i);
				if (key != null && !queryDict.ContainsKey(key))
					queryDict.Add(key, value);
				else if (!queryDict.ContainsKey(value))
					queryDict.Add(value, null);
			}

			var parameters = new Dictionary<string, object>();
			parameters.Add("Browser", browser.Browser);
			parameters.Add("BrowserType", browser.Type);
			parameters.Add("BrowserVersion", browser.Version);
			parameters.Add("Platform", browser.Platform);
			parameters.Add("IsMobileDevice", browser.IsMobileDevice);
			parameters.Add("MobileDeviceManufacturer", browser.MobileDeviceManufacturer);
			parameters.Add("MobileDeviceModel", browser.MobileDeviceModel);
			parameters.Add("UserAgent", request.UserAgent);
			parameters.Add("UserHostAddress", request.UserHostAddress);
			parameters.Add("UserHostName", request.UserHostName);
			parameters.Add("UserLanguages", request.UserLanguages);
			parameters.Add("UrlReferrer", request.UrlReferrer);
			parameters.Add("QueryString", queryDict);

			var info = _parser.Parse(request.UserAgent);

			parameters.Add("UserAgentInfo", info.ToJsonString());
			
			var location = GeoipService.GetLocation(request.UserHostAddress);
			parameters.Add("Location", location);
			
			return parameters;
		}
	}
}