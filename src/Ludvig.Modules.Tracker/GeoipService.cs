﻿using MaxMind.GeoIP2;
using MaxMind.GeoIP2.Responses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Reflection;

namespace Hermes.Modules.Tracker
{
    public class GeoipService
    {
        private static DatabaseReader _reader;

        static GeoipService()
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            var directory = Path.GetDirectoryName(path);
            var dbPath = Path.Combine(directory, @"Geoip\GeoLite2-City.mmdb");
            if(!File.Exists(dbPath))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(dbPath));
                var client = new HttpClient();
                using(var fileStream = new FileStream(dbPath, FileMode.Create))
                {
                    using (var httpStream = client.GetStreamAsync("https://campaignops.blob.core.windows.net/files/geoip/GeoLite2-City.mmdb").Result)
                    {
                        httpStream.CopyTo(fileStream);
                    }
                }
            }

            _reader = new DatabaseReader(dbPath);
        }
        public static Dictionary<string, object> GetLocation(string ip)
        {
            CityResponse city;
            if (!_reader.TryCity(ip, out city))
                return null;

            var location = new Dictionary<string, object>();

            if (!string.IsNullOrEmpty(city.Country.Name))
                location.Add("Country", city.Country.Name);
            if (!string.IsNullOrEmpty(city.City.Name))
                location.Add("City", city.City.Name);
            if (city.Location.HasCoordinates)
                location.Add("Coordinates", new { Lat = city.Location.Latitude, Lon = city.Location.Longitude });

            return location;
        }
    }
}