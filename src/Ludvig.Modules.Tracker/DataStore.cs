﻿using Hermes.DataAccess.Databases;
using Hermes.DataAccess.Databases.Couchbase;
using Hermes.Utilities.Ioc;
using System.Threading.Tasks;

namespace Hermes.Modules.Tracker
{
    public class DataStore
    {
        private static readonly IDocumentStore<string, object, TrackerBucketNameContainer> _documentStore;

        static DataStore()
        {
            _documentStore = GlobalServiceResolver.Resolve<IDocumentStore<string, object, TrackerBucketNameContainer>>();
        }

        public static Task UpsertAsync(string key, object value)
        {
            return _documentStore.UpdateAsync(key, value);
        }
    }
}