﻿using Ludvig.Utilities.Ioc;

namespace Ludvig.Utilities.Mapping
{
	[RegisterAll]
	public interface IBindingMapper
	{
		void Bind(IBindingContex contex);
	}
}