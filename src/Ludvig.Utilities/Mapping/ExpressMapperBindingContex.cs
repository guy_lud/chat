﻿using System;
using ExpressMapper;

namespace Ludvig.Utilities.Mapping
{
	public class ExpressMapperBindingContex : IBindingContex
	{
		private readonly IMappingServiceProvider _mapper = Mapper.Instance;

		public void Bind<TSource, TTarget>(Action<IBindingConfiguration<TSource, TTarget>> configAction = null)
		{
			var config = _mapper.Register<TSource, TTarget>();

			if (configAction == null)
				return;

			var bind = new ExpressMapperBindingConfiguration<TSource, TTarget>(config);

			configAction(bind);
		}

		public void SimpleBind<TSource, TTarget>(Func<TSource, TTarget> mapFunc)
		{
			_mapper.RegisterCustom(mapFunc);
		}

		public void Compile()
		{
			_mapper.Compile();
		}
	}
}