﻿namespace Ludvig.Utilities.Mapping
{
	public interface IObjectMapper
	{
		TTarget Map<TSource, TTarget>(TSource source);
	}
}
