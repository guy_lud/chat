using System;
using System.Linq.Expressions;
using ExpressMapper;

namespace Ludvig.Utilities.Mapping
{
	public class ExpressMapperBindingConfiguration<TSource, TTarget> : IBindingConfiguration<TSource, TTarget>
	{
		private readonly IMemberConfiguration<TSource, TTarget> _config;

		public ExpressMapperBindingConfiguration(IMemberConfiguration<TSource, TTarget> config)
		{
			_config = config;
		}

		public IBindingConfiguration<TSource, TTarget> Member(Expression<Func<TSource, object>> source, Expression<Func<TTarget, object>> target)
		{
			_config.Member(target, source);
			return this;
		}

		public IBindingConfiguration<TSource, TTarget> AlterMember(Func<TSource, object> source, Expression<Func<TTarget, object>> target)
		{
			_config.Function(target, source);
			return this;
		}

		public IBindingConfiguration<TSource, TTarget> Ignore(Expression<Func<TTarget, object>> source)
		{
			_config.Ignore(source);
			return this;
		}

		public IBindingConfiguration<TSource, TTarget> PostMapping(Action<TSource,TTarget> postAction)
		{
			_config.After(postAction);
			return this;
		}

		public IBindingConfiguration<TSource, TTarget> Member(Action<TSource, TTarget> action)
		{
			_config.Before(action);

			return this;
		}
	}
}