﻿using System;
using System.Linq.Expressions;

namespace Ludvig.Utilities.Mapping
{
	public interface IBindingConfiguration<TSource, TTarget>
	{
		IBindingConfiguration<TSource, TTarget> Member(Expression<Func<TSource, object>> source, Expression<Func<TTarget, object>> target);
		IBindingConfiguration<TSource, TTarget> AlterMember(Func<TSource, object> source, Expression<Func<TTarget, object>> target);
		IBindingConfiguration<TSource, TTarget> Ignore(Expression<Func<TTarget, object>> source);
		IBindingConfiguration<TSource, TTarget> PostMapping(Action<TSource, TTarget> postAction);
	}
}