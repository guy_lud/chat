using System;
using Ludvig.Utilities.Ioc;

namespace Ludvig.Utilities.Mapping
{
	[SkipAutoRegistration]
	public interface IBindingContex
	{
		void Bind<TSource, TTarget>(Action<IBindingConfiguration<TSource, TTarget>> configAction = null);
		void SimpleBind<TSource, TTarget>(Func<TSource, TTarget> mapFunc);
	}
}