using ExpressMapper;

namespace Ludvig.Utilities.Mapping
{
	internal class ObjectMapper : IObjectMapper
	{
		public TTarget Map<TSource, TTarget>(TSource source)
		{
			return Mapper.Map<TSource, TTarget>(source);
		}
	}
}