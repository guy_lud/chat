﻿using System;
using System.Runtime.Serialization;

namespace Ludvig.Utilities.Exceptions
{
	[Serializable]
	public class FatalErrorException : Exception
	{
		public FatalErrorException(string message) 
			: base(message)
		{
		}

		public FatalErrorException(string message, Exception inner) : base(message, inner)
		{
		}

		protected FatalErrorException(
			SerializationInfo info,
			StreamingContext context) : base(info, context)
		{
		}
	}
}
