﻿namespace Ludvig.Utilities.Ioc
{
	public interface IServiceRegistrationAction
	{
		void Register(IServiceRegistrator serviceRegistrator);
	}
}