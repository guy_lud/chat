﻿using System;

namespace Ludvig.Utilities.Ioc
{
	[AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
	public class RegisterAllAttribute : Attribute
	{
	}
}
