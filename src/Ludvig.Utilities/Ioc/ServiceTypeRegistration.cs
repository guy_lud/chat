﻿using System;

namespace Ludvig.Utilities.Ioc
{
	public class ServiceTypeRegistration : IServiceRegistrationAction
	{
		private readonly Type _serviceType;
		private readonly Type _serviceImplementation;

		public ServiceTypeRegistration(Type serviceType, Type serviceImplementation)
		{
			_serviceType = serviceType;
			_serviceImplementation = serviceImplementation;
		}

		public void Register(IServiceRegistrator serviceRegistrator)
		{
			serviceRegistrator.Register(_serviceType, _serviceImplementation);
		}
	}
}