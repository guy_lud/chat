﻿using System;

namespace Ludvig.Utilities.Ioc
{
	public class InstanceTypeRegistration : IServiceRegistrationAction
	{
		private readonly Type _serviceType;
		private readonly object _instance;

		public InstanceTypeRegistration(Type serviceType, object instance)
		{
			_serviceType = serviceType;
			_instance = instance;
		}

		public void Register(IServiceRegistrator serviceRegistrator)
		{
			serviceRegistrator.Register(_serviceType, _instance);
		}
	}
}
