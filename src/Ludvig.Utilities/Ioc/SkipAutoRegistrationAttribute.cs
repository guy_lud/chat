﻿using System;

namespace Ludvig.Utilities.Ioc
{
	[AttributeUsage(AttributeTargets.Interface)]
	public class SkipAutoRegistrationAttribute : Attribute
	{
	}
}