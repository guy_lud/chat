using System.Linq;

namespace Ludvig.Utilities.Ioc
{
	public static class ServiceResolverExtensions
	{
		public static T Resolve<T>(this IServiceResolver target)
		{
			return (T) target.Resolve(typeof (T));
		}

		public static T[] ResolveAll<T>(this IServiceResolver target)
		{
			return target.ResolveAll(typeof(T)).Cast<T>().ToArray();
		}
	}
}