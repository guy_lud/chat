using System;
using System.Linq;

namespace Ludvig.Utilities.Ioc
{
	public static class GlobalServiceResolver
	{
		private static IServiceResolver ServiceResolver { get; set; }

		public static void SetResolver(IServiceResolver serviceResolver)
		{
			ServiceResolver = serviceResolver;
		}

		public static object Resolve(Type type)
		{
			return ServiceResolver.Resolve(type);
		}

		public static T Resolve<T>()
		{
			return (T)ServiceResolver.Resolve(typeof(T));
		}

		public static object[] ResolveAll(Type type)
		{
			return ServiceResolver.ResolveAll(type).ToArray();
		}

		public static T[] ResolveAll<T>()
		{
			return ServiceResolver.ResolveAll(typeof(T)).Cast<T>().ToArray();
		}
	}
}