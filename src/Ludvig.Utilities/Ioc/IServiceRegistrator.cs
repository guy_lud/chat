﻿using System;

namespace Ludvig.Utilities.Ioc
{
	public interface IServiceRegistrator
	{
		IServiceRegistrator Register(Type serviceType, Type implementationType);
		IServiceRegistrator Register(Type serviceType, object instance);
	}
}