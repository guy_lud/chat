﻿using System;
using System.Collections.Generic;

namespace Ludvig.Utilities.Ioc
{
	[SkipAutoRegistration]
	public interface IServiceResolver
	{
		object Resolve(Type service);
		object[] ResolveAll(Type service);
		T Resolve<T>() where T : class;
		IEnumerable<T> ResolveAll<T>() where T : class;
		IList<object> FindRegisteredInstances(Func<Type, bool> predicate);
	}
}