﻿using System;

namespace Ludvig.Utilities.Ioc
{
	public abstract class EnvironmentAttribute : Attribute
	{
		public string EnvironmentName { get; set; }

		protected EnvironmentAttribute(string environmentName)
		{
			EnvironmentName = environmentName;
		}
	}

	public class DevEnvironment : EnvironmentAttribute
	{
		public DevEnvironment() 
			: base("Development")
		{
		}
	}
}
