﻿using Ludvig.Utilities.Ioc;
using Ludvig.Utilities.Misc;

namespace Ludvig.Utilities.Configuration
{
	[SkipAutoRegistration]
	public interface IHostingEnvironment
	{
		IConfigurationManager Configuration { get; }
		string EnvironmentName { get; }
		string ApplicationRootPath { get; set; }
	}

	public class HostingEnvironment : IHostingEnvironment
	{
		public IConfigurationManager Configuration { get; }
		public string ApplicationRootPath { get; set; }

		private const string DefaultEnv = "production";

		public HostingEnvironment(IConfigurationManager configurationManager, string applicationRootPath)
		{
			Configuration = configurationManager;
			ApplicationRootPath = applicationRootPath;
		}

		public string EnvironmentName => Configuration.AppSettings["hosting:environment"] ?? DefaultEnv;
	}

	public static class HostingEnvironmentExtensions
	{
		public static bool IsProduction(this IHostingEnvironment target)
		{
			return target.EnvironmentName.EqualsIgnoreCase("production");
		}

		public static bool IsDevelopment(this IHostingEnvironment target)
		{
			return target.EnvironmentName.EqualsIgnoreCase("development");
		}

		public static string SqliteDatabasePath(this IHostingEnvironment target)
		{
			return target.Configuration.AppSettings["ChatDBPath"];
		}
	}
}
