﻿namespace Ludvig.Utilities.Configuration
{
	public interface IConnectionStringProvider
	{
		string GetConnection(string databaseName);
	}
}
