﻿using System.Collections.Specialized;
using System.Configuration;

namespace Ludvig.Utilities.Configuration
{
	public interface IConfigurationManager
	{
		ConnectionStringSettingsCollection ConnectionStrings { get; }

		NameValueCollection AppSettings { get; }
	}

	public class SystemConfigurationManager : IConfigurationManager
	{
		public ConnectionStringSettingsCollection ConnectionStrings => ConfigurationManager.ConnectionStrings;

		public NameValueCollection AppSettings => ConfigurationManager.AppSettings;
	}

	public static class ConfigrationManagerExtensions
	{
		public static string GetMongoDbConnectionString(this IConfigurationManager target)
		{
			return target.ConnectionStrings?["CamOps"].ConnectionString;
		}
	}
}
