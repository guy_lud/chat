﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ludvig.Utilities.Defence
{
	public static class NullCheckingExtensions
	{
		public static void ThrowIfNull(this object target, string paramName = null)
		{
			if (target != null)
				return;

			if (paramName == null)
				throw new ArgumentNullException();

			throw new ArgumentNullException(paramName);
		}

		public static bool IsNullOrEmpty<T1>(this IEnumerable<T1> target)
		{
			return target == null || !target.Any();
		}
	}
}