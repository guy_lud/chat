using System;
using System.Linq;
using Ludvig.Utilities.Defence;

namespace Ludvig.Utilities.Caching
{
	public static class CacherExtensions
	{
		public static T Get<T>(this IRequestLevelCacher target, string key)
		{
			var items = target.Get<T>(new[] { key });
			if (items.IsNullOrEmpty())
				return default(T);

			return items.First();
		}

		public static T Get<T>(this IRequestLevelCacher target, string key, Func<T> creator, TimeSpan? cacheDuration = null)
		{
			var items = target.Get<T>(new[] { key });
			if (!items.IsNullOrEmpty())
				return items.First();

			var newItem = creator();
			target.Save(key, newItem, cacheDuration);
			return newItem;
		}

		public static void Invalidate(this IRequestLevelCacher target, string key)
		{
			target.Invalidate(new[] { key });
		}
	}
}