﻿using System;
using System.Collections.Generic;

namespace Ludvig.Utilities.Caching
{
	public interface IRequestLevelCacher
	{
		ICollection<T> Get<T>(IEnumerable<string> keys);
		void Save<T>(string key, T item, TimeSpan? cacheDuration = null);
		void Invalidate(IEnumerable<string> keys);
		void InvalidateAll();
	}
}