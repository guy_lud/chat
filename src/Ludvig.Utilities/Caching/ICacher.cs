﻿using System;
using System.Collections.Generic;

namespace Ludvig.Utilities.Caching
{
	public interface ICacher
	{
		CacheItemsCollection Get(IEnumerable<string> keys);
		T Get<T>(string key);
		bool Save<T>(string key, T item, TimeSpan? cacheDuration = null);
		void AddOrUpdate(string key, object item, TimeSpan? cacheDuration = null);
		void Invalidate(IEnumerable<string> keys);
		void Invalidate(string keys);
	}
}