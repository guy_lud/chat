﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;
using Ludvig.Utilities.DateTime;

namespace Ludvig.Utilities.Caching
{
	public interface IInMemoryCacher : ICacher
	{
		bool Contains(string key);
		T AddOrExisting<T>(string key, Func<T> valueFactory, TimeSpan? cacheDuration = null);
	}

	public class InMemoryCacher : IInMemoryCacher
	{
		private readonly ISystemTime _systemTime;
		private readonly ObjectCache _cache = MemoryCache.Default;

		public InMemoryCacher(ISystemTime systemTime)
		{
			_systemTime = systemTime;
		}

		public CacheItemsCollection Get(IEnumerable<string> keys)
		{
			return new CacheItemsCollection(_cache.GetValues(keys));
		}

		public T Get<T>(string key)
		{
			var val = _cache.Get(key);
			return val is T ? (T)val : default(T);
		}

		public bool Save<T>(string key, T item, TimeSpan? cacheDuration = null)
		{
			return _cache.Add(key, item, GetCacheStalenessTime(cacheDuration));
		}

		public void AddOrUpdate(string key, object item, TimeSpan? cacheDuration = null)
		{
			_cache.Set(key, item, GetCacheStalenessTime(cacheDuration));
		}

		public void Invalidate(IEnumerable<string> keys)
		{
			foreach (var key in keys)
			{
				_cache.Remove(key);
			}
		}

		public void Invalidate(string key)
		{
			_cache.Remove(key);
		}

		public bool Contains(string key)
		{
			return _cache.Contains(key);
		}

		public T AddOrExisting<T>(string key, Func<T> valueFactory, TimeSpan? cacheDuration = null)
		{
			var newValue = valueFactory;
			var oldValue = _cache.AddOrGetExisting(key, newValue, GetCacheStalenessTime(cacheDuration));
			try
			{
				return (T) (oldValue ?? newValue);
			}
			catch
			{
				_cache.Remove(key);
				throw;
			}
		}

		private DateTimeOffset GetCacheStalenessTime(TimeSpan? cacheDuration)
		{
			return cacheDuration.HasValue ? new DateTimeOffset(_systemTime.Now, cacheDuration.Value) : DateTimeOffset.MaxValue;
		}
	}

}