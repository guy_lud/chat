using System.Collections.Generic;

namespace Ludvig.Utilities.Caching
{
	public class CacheItemsCollection : Dictionary<string, object>
	{
		public CacheItemsCollection(IDictionary<string, object> dictionary) 
			: base(dictionary)
		{
		}
	}
}