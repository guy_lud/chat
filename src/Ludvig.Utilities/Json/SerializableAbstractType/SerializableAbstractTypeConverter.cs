using System;
using System.Linq;
using Ludvig.Utilities.Defence;
using Ludvig.Utilities.Formatting;
using Ludvig.Utilities.Ioc;
using Ludvig.Utilities.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Ludvig.Utilities.Json.SerializableAbstractType
{
	public class SerializableAbstractTypeConverter : JsonConverter
	{
		private readonly Lazy<IImplementationTypeProvider[]> _implementationTypeProviders;
		private readonly Lazy<IImplementationTypeProvider> _defaultImplementationTypeProvider;

		public SerializableAbstractTypeConverter()
		{
			_implementationTypeProviders = new Lazy<IImplementationTypeProvider[]>(() => GlobalServiceResolver.ResolveAll<IImplementationTypeProvider>());
			_defaultImplementationTypeProvider = new Lazy<IImplementationTypeProvider>(() => _implementationTypeProviders.Value.OfType<DefaultImplementationTypeProvider>().Single());
		}

		public override bool CanConvert(Type objectType)
		{
			return objectType.HasCustomAttribute<SerializableAbstractTypeAttribute>();
		}

		public override object ReadJson(JsonReader reader,
			Type objectType, object existingValue, JsonSerializer serializer)
		{
			var keyName = objectType.GetAttributeValue<SerializableAbstractTypeAttribute, string>(a => a.TypeFieldName);
			if (keyName.IsNullOrEmpty())
				throw new InvalidOperationException();

			var item = JObject.Load(reader);

			var typeKey = item[keyName.FromPascalTocamlCase()];
			if (typeKey == null)
				throw new InvalidOperationException();

			var implementationTypeProviderType = objectType.GetAttributeValue<SerializableAbstractTypeAttribute, Type>(s => s.TypeKeyProvider);
			var provider = implementationTypeProviderType == null
				? _defaultImplementationTypeProvider.Value
				: _implementationTypeProviders.Value.FirstOrDefault(x => x.GetType() == implementationTypeProviderType);

			if (provider == null)
				throw new InvalidOperationException();

			var implementationType = provider.GetImplementationType(objectType, keyName, typeKey.Value<string>());
			return item.ToObject(implementationType);
		}

		public override bool CanWrite => false;
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}
	}
}