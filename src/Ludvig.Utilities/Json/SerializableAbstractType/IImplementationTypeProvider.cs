using System;
using Ludvig.Utilities.Ioc;

namespace Ludvig.Utilities.Json.SerializableAbstractType
{
	[RegisterAll]
	public interface IImplementationTypeProvider
	{
		Type GetImplementationType(Type contractType, string typeFieldName, string typeValue);
	}
}