using System;
using System.Linq;
using Ludvig.Utilities.Ioc;

namespace Ludvig.Utilities.Json.SerializableAbstractType
{
	public class DefaultImplementationTypeProvider : IImplementationTypeProvider
	{
		public Type GetImplementationType(Type contractType, string typeFieldName, string typeValue)
		{
			var possibleTypes = GlobalServiceResolver.ResolveAll(contractType);

			var property = contractType.GetProperty(typeFieldName);

			return possibleTypes.FirstOrDefault(x => (string)property.GetValue(x) == typeValue)?.GetType();
		}
	}
}