using System;
using Ludvig.Utilities.Ioc;

namespace Ludvig.Utilities.Json.SerializableAbstractType
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct, Inherited = false)]
	public class SerializableAbstractTypeAttribute : RegisterAllAttribute
	{
		public string TypeFieldName { get; }
		public Type TypeKeyProvider { get; }

		public SerializableAbstractTypeAttribute(string typeFieldName)
		{
			TypeFieldName = typeFieldName;
		}

		public SerializableAbstractTypeAttribute(string typeFieldName, Type typeKeyProvider) : this(typeFieldName)
		{
			if (!typeof(IImplementationTypeProvider).IsAssignableFrom(typeKeyProvider))
				throw new ArgumentException();

			TypeKeyProvider = typeKeyProvider;
		}
	}
}