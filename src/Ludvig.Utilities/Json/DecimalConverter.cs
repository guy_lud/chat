﻿using System;
using Newtonsoft.Json;

namespace Ludvig.Utilities.Json
{
    public class DecimalConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToString());
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            decimal val = 0;
            if (reader.Value != null)
                decimal.TryParse(reader.Value.ToString(), out val);
            return val;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(decimal);
        }
    }
}
