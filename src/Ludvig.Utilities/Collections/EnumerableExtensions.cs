﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ludvig.Utilities.Collections
{
	public static class EnumerableExtensions
	{
		public static void ForEach<T>(this IEnumerable<T> target, Action<T> actionToPerform)
		{
			foreach (var item in target)
			{
				actionToPerform(item);
			}
		}

		public static IEnumerable<T> EnumerableForEach<T>(this IEnumerable<T> target, Action<T> actionToPerform)
		{
			var enumerator = target.GetEnumerator();

			while (enumerator.MoveNext())
			{
				actionToPerform(enumerator.Current);
				yield return enumerator.Current;
			}
		}

		public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T> target)
		{
			return target.Where(x => x != null);
		}

		public static bool In<T>(this T target, IEnumerable<T> collection)
		{
			return collection.Contains(target);
		}
	}
}
