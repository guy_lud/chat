﻿namespace Ludvig.Utilities.Collections
{
	public static class ObjectExtentsions
	{
		public static T[] AsArray<T>(this T target)
		{
			return new [] { target };
		}
	}
}
