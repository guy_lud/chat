using System.Collections.Generic;

namespace Ludvig.Utilities.Logging
{
	public interface ILogValues
	{
		IEnumerable<KeyValuePair<string, object>> GetValues();
	}
}