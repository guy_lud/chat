using System;

namespace Ludvig.Utilities.Logging
{
	public interface ILoggerProvider : IDisposable
	{
		ILogger CreateLogger(string categoryName);
	}
}