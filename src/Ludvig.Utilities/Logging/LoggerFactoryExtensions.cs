using System;

namespace Ludvig.Utilities.Logging
{
	public static class LoggerFactoryExtensions
	{
		public static ILogger CreateLogger<T>(this ILoggerFactory factory)
		{
			if (factory == null)
			{
				throw new ArgumentNullException(nameof(factory));
			}

			return factory.CreateLogger(typeof(T).FullName);
		}
	}
}