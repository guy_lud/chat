﻿using System;
using Ludvig.Utilities.Ioc;

namespace Ludvig.Utilities.Logging
{
	public interface ILogger<out TCategoryName> : ILogger
	{

	}

	[SkipAutoRegistration]
	public interface ILogger
	{
		void Log(LogLevel logLevel, int eventId, object state, Exception exception, Func<object, Exception, string> formatter);
		
		bool IsEnabled(LogLevel logLevel);

		IDisposable BeginScopeImpl(object state);
	}
}