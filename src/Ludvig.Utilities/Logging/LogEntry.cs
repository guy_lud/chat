namespace Ludvig.Utilities.Logging
{
	public class LogEntry
	{
		public string Logger { get; set; }
		public long? Timestamp { get; set; }
		public string Level { get; set; }
		public string Url { get; set; }
		public string[] Message { get; set; }
		public string Exception { get; set; }
	}
}