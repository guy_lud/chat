using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Ludvig.Utilities.Logging
{
	class FormattedLogValues : ILogValues
	{
		private static ConcurrentDictionary<string, LogValuesFormatter> _formatters = new ConcurrentDictionary<string, LogValuesFormatter>();
		private readonly LogValuesFormatter _formatter;
		private readonly object[] _values;

		public FormattedLogValues(string format, params object[] values)
		{
			_formatter = _formatters.GetOrAdd(format, f => new LogValuesFormatter(f));
			_values = values;
		}

		public IEnumerable<KeyValuePair<string, object>> GetValues()
		{
			return _formatter.GetValues(_values);
		}

		public override string ToString()
		{
			return _formatter.Format(_values);
		}
	}
}