﻿using System;
using Ludvig.Utilities.Ioc;

namespace Ludvig.Utilities.Logging
{
	[SkipAutoRegistration]
	public interface ILoggerFactory : IDisposable
	{
		LogLevel MinimumLevel { get; set; }

		ILogger CreateLogger(string categoryName);
		
		void AddProvider(ILoggerProvider provider);
	}
}