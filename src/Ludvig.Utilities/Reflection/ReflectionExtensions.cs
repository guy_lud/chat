﻿using System;
using System.Linq;
using System.Reflection;

namespace Ludvig.Utilities.Reflection
{
    public static class ReflectionExtensions
    {
        public static bool HasCustomAttribute<T>(this Type target) where T : Attribute
        {
            return target.GetCustomAttribute<T>() != null;
        }

        public static TValue GetAttributeValue<TAttribute, TValue>(this Type type, Func<TAttribute, TValue> valueSelector) where TAttribute : Attribute
        {
            var att = type.GetCustomAttributes(typeof(TAttribute), true).FirstOrDefault() as TAttribute;

            if (att != null)
                return valueSelector(att);

            return default(TValue);
        }
    }
}
