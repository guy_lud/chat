﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Ludvig.Utilities.Web
{
    public class UrlRedirectResolver
    {
        public static async Task<string> ResolveFinalRedirectAsync(string url)
        {
            try
            {
                var req = WebRequest.CreateHttp(url);
                req.AllowAutoRedirect = true;
                var res = await req.GetResponseAsync();
                return res.ResponseUri.AbsoluteUri;
            }
            catch
            {
                // TODO: logging
                Console.WriteLine("Couldn't resolve '{0}' directly, trying to walk the path.", url);
            }

            return null;
        }

        public static async Task<IList<string>> ResolveAllRedirects(string url)
        {
            var redirects = new List<string>();
            string location = string.Copy(url);
            while (!string.IsNullOrWhiteSpace(location))
            {
                redirects.Add(location);

                try
                {
                    var request = WebRequest.CreateHttp(location);
                    request.AllowAutoRedirect = false;

                    using (var response = (HttpWebResponse)await request.GetResponseAsync())
                    {
                        location = response.GetResponseHeader("Location");
                    }
                }
                catch
                {
                    // TODO: logging
                    Console.WriteLine("Couldn't resolve '{0}' hop while walking redirect path.", location);
                    location = null;
                }
            }
            return redirects;
        }
    }
}
