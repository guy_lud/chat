﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Ludvig.Utilities.Web
{
	// This project can output the Class library as a NuGet Package.
	// To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
	public static class HttpContentExtensions
	{
		public static Task ReadAsFileAsync(this HttpContent content, string filename, bool overwrite)
		{
			string pathname = Path.GetFullPath(filename);
			if (!overwrite && File.Exists(filename))
			{
				throw new InvalidOperationException(string.Format("File {0} already exists.", pathname));
			}

			FileStream fileStream = null;
			try
			{
				fileStream = new FileStream(pathname, FileMode.Create, FileAccess.Write, FileShare.None);
				return content.CopyToAsync(fileStream).ContinueWith(
					(copyTask) =>
					{
						fileStream.Close();
					});
			}
			catch
			{
				if (fileStream != null)
				{
					fileStream.Close();
				}

				throw;
			}
		}
	}
}
