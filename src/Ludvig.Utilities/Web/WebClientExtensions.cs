﻿using System.Net;

namespace Ludvig.Utilities.Web
{
	public static class WebClientExtensions
	{
		public static WebClient SetFormContentType(this WebClient target)
		{
			target.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
			return target;
		}
	}
}