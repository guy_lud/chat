﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Ludvig.Utilities.Crypto
{
	public interface IAesEncryptor
	{
		string Encrypt(string content, string customKey = null);
		string Decrypt(string encryptedContent, string customKey = null);
	}

	public class AesEncryptor : IAesEncryptor
	{
		public string Encrypt(string content, string customKey = null)
		{
			if (content == null)
				return null;

			using (var myRijndael = new RijndaelManaged())
			{
				var keyBytes = GetKeyBytes(customKey);

				myRijndael.GenerateIV();
				var iv = myRijndael.IV;
				// Encrypt the string to an array of bytes. 
				var encryptedBytes = EncryptStringToBytes(content, keyBytes, iv).ToList();
				encryptedBytes.InsertRange(0, iv);

				var encryptedContent = Convert.ToBase64String(encryptedBytes.ToArray());
				return encryptedContent;
			}
		}

		public string Decrypt(string encryptedContent, string customKey = null)
		{
			if (encryptedContent == null)
				return null;

			var keyBytes = GetKeyBytes(customKey);

			var bytes = Convert.FromBase64String(encryptedContent);
			var iv = bytes.Take(16).ToArray();
			var encryptedBytes = bytes.Skip(16).ToArray();
			
			return DecryptStringFromBytes(encryptedBytes, keyBytes, iv);
		}

		private static byte[] EncryptStringToBytes(string plainText, byte[] key, byte[] iv)
		{
			// Check arguments. 
			if (plainText == null || plainText.Length <= 0)
				throw new ArgumentNullException(nameof(plainText));
			if (key == null || key.Length <= 0)
				throw new ArgumentNullException(nameof(key));
			if (iv == null || iv.Length <= 0)
				throw new ArgumentNullException(nameof(iv));
			
			// Create an RijndaelManaged object 
			// with the specified key and IV. 
			using (var rijAlg = new RijndaelManaged())
			{
				rijAlg.Key = key;
				rijAlg.IV = iv;

				// Create a decryptor to perform the stream transform.
				var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

				// Create the streams used for encryption. 
				using (var msEncrypt = new MemoryStream())
					using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
					{
						using (var swEncrypt = new StreamWriter(csEncrypt))
							swEncrypt.Write(plainText);

						return msEncrypt.ToArray();
					}
			}
		}

		private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
		{
			// Check arguments. 
			if (cipherText == null || cipherText.Length <= 0)
				throw new ArgumentNullException(nameof(cipherText));
			if (key == null || key.Length <= 0)
				throw new ArgumentNullException(nameof(key));
			if (iv == null || iv.Length <= 0)
				throw new ArgumentNullException(nameof(iv));

			// Create an RijndaelManaged object 
			// with the specified key and IV. 
			using (var rijAlg = new RijndaelManaged())
			{
				rijAlg.Key = key;
				rijAlg.IV = iv;

				// Create a decrytor to perform the stream transform.
				var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

				// Create the streams used for decryption. 
				using (var msDecrypt = new MemoryStream(cipherText))
					using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
						using (var srDecrypt = new StreamReader(csDecrypt))
							return srDecrypt.ReadToEnd();
			}

		}

		private byte[] GetKeyBytes(string key)
		{
            const int size = 32;

			var keyBytes = Encoding.UTF8.GetBytes(key).ToList();

			if (keyBytes.Count == size)
				return keyBytes.ToArray();

			if (keyBytes.Count > size)
				return keyBytes.Take(size).ToArray();

			var additionalArray = Enumerable.Repeat(new byte(), size - keyBytes.Count).ToList();
			keyBytes.AddRange(additionalArray);
			return keyBytes.ToArray();
		}
	}

	public static class AesEncryptorExtensions
	{
		public static string DecryptOrNull(this IAesEncryptor target, string content, string customKey = null)
		{
			try
			{
				return target.Decrypt(content, customKey);
			}
			catch
			{
				return null;
			}
		}
	}
}