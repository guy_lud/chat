﻿using System;
using System.Security.Cryptography;

namespace Ludvig.Utilities.Crypto
{
	public interface IPasswordHasher : IHasher
	{
		bool ValidatePassword(string password, string correctHash);
	}

	public class PasswordHasher : IPasswordHasher
	{
		public const int SaltByteSize = 24;
		public const int HashByteSize = 24;
		public const int Pbkdf2Iterations = 1000;

		public const int IterationIndex = 0;
		public const int SaltIndex = 1;
		public const int Pbkdf2Index = 2;

		public string GetHash(string password)
		{
			using (var csprng = new RNGCryptoServiceProvider())
			{
				var saltBytes = new byte[SaltByteSize];
				csprng.GetBytes(saltBytes);

				var hashBytes = Pbkdf2(password, saltBytes, Pbkdf2Iterations, HashByteSize);
				return Pbkdf2Iterations + ":" +
					Convert.ToBase64String(saltBytes) + ":" +
					Convert.ToBase64String(hashBytes);
			}
		}

		public bool ValidatePassword(string password, string correctHash)
		{
			var delimiter = new[] { ':' };
			var split = correctHash.Split(delimiter);
			var iterations = int.Parse(split[IterationIndex]);
			var salt = Convert.FromBase64String(split[SaltIndex]);
			var hash = Convert.FromBase64String(split[Pbkdf2Index]);

			var testHash = Pbkdf2(password, salt, iterations, hash.Length);
			return SlowEquals(hash, testHash);
		}
		
		private bool SlowEquals(byte[] a, byte[] b)
		{
			var diff = (uint)a.Length ^ (uint)b.Length;
			for (var i = 0; i < a.Length && i < b.Length; i++)
				diff |= (uint)(a[i] ^ b[i]);
			return diff == 0;
		}
		
		private static byte[] Pbkdf2(string password, byte[] salt, int iterations, int outputBytes)
		{
			var pbkdf2 = new Rfc2898DeriveBytes(password, salt) { IterationCount = iterations };
			return pbkdf2.GetBytes(outputBytes);
		}
	}
}