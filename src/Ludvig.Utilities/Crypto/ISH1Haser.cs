﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Ludvig.Utilities.Crypto
{
	public class SH1Haser : ISH1Haser
	{
		public string GetHash(string source, bool removeDash = true)
		{
			if (source == null) throw new ArgumentNullException(nameof(source));

			var bytes = StringToBytes(source);

			using (var sha = new SHA1CryptoServiceProvider())
			{
				var result = BitConverter.ToString(sha.ComputeHash(bytes));
				if (removeDash)
					result = result.Replace("-", "");

				return result;
			}
		}

		private byte[] StringToBytes(string str)
		{
			return Encoding.UTF8.GetBytes(str);
		}
	}

	public interface ISH1Haser
	{
		string GetHash(string source, bool removeDash = true);
	}
}