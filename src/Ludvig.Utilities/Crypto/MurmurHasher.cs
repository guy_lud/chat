﻿using System.Security.Cryptography;
using System.Text;
using Murmur;

namespace Ludvig.Utilities.Crypto
{
	public interface IMurmurHasher : IHasher
	{
	}

	public class MurmurHasher : IMurmurHasher
	{
        private static readonly HashAlgorithm Murmur128;

        static MurmurHasher()
        {
            Murmur128 = MurmurHash.Create128(managed: false);
        }

	    public string GetHash(string text)
	    {
            if (text == null)
                return null;

            var data = Encoding.UTF8.GetBytes(text);
            byte[] hash;
            lock(Murmur128)
            {
                Murmur128.Initialize();
                hash = Murmur128.ComputeHash(data);
            }

            StringBuilder sBuilder = new StringBuilder();

            foreach (byte t in hash)
            {
	            sBuilder.Append(t.ToString("x2"));
            }

	        return sBuilder.ToString();
        }
    }
}
