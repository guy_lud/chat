﻿using System.Security.Cryptography;
using System.Text;

namespace Ludvig.Utilities.Crypto
{
	public interface IMd5Hasher : IHasher
	{
	}

	public class Md5Hasher : IMd5Hasher
	{
		private static readonly MD5 Md5;

		static Md5Hasher()
		{
			Md5 = MD5.Create();
		}

		public string GetHash(string text)
		{
			byte[] data = Md5.ComputeHash(Encoding.UTF8.GetBytes(text));

			StringBuilder sBuilder = new StringBuilder();

			foreach (byte t in data)
			{
				sBuilder.Append(t.ToString("x2"));
			}

			return sBuilder.ToString();
		}
	}
}
