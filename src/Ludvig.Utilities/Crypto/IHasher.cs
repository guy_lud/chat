﻿namespace Ludvig.Utilities.Crypto
{
	public interface IHasher
	{
		string GetHash(string text);
	}
}