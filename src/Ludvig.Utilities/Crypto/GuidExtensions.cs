﻿using System;
using System.Linq;

namespace Ludvig.Utilities.Crypto
{
	public static class GuidExtensions
	{
		public static  string ToShortKey(this Guid target, int length = 20)
		{
			//This will result in a 11 chars, case sensitive unique ID,
			//containing A-Z, a-z, 0-9, -, and _
			var i = target.ToByteArray().Aggregate<byte, long>(1, (current, b) => current * (b + 1));
			var bytes = BitConverter.GetBytes(i - System.DateTime.Now.Ticks);
			return Convert.ToBase64String(bytes).TrimEnd('=').Replace("+", "-").Replace("/", "_");
		}
	}
}