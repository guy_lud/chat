﻿namespace Ludvig.Utilities.DateTime
{
	public interface ISystemTime
	{
		System.DateTime Now { get; }
	}

	public class SystemTime : ISystemTime
	{
		public System.DateTime Now => System.DateTime.UtcNow;
	}
}
