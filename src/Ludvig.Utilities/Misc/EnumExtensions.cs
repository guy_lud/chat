﻿using System;

namespace Ludvig.Utilities.Misc
{
	public static class EnumExtensions
	{
		public static TEnum ParseEnum<TEnum>(this string target, bool ignoreCase = true, bool isDefaultIfNull = true) where TEnum : struct, IConvertible, IFormattable, IComparable
		{
			if (!typeof(TEnum).IsEnum)
			{
				throw new ArgumentException("T must be an enumerated type");
			}

			if (target == null)
			{
				if (isDefaultIfNull)
					return default(TEnum);

				throw new ArgumentNullException(nameof(target));
			}

			var result = Enum.Parse(typeof(TEnum), target, ignoreCase);
			return (TEnum)result;
		}
	}
}