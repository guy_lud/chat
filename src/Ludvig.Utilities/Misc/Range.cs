﻿using System;

namespace Ludvig.Utilities.Misc
{
    public enum RangeBoundaryType
    {
        Inclusive = 0,
        Exclusive
    }

    public class Range<T> : IComparable<Range<T>>, IEquatable<Range<T>>
        where T : struct, IComparable<T>
    {
        public T Min { get; set; }
        public T Max { get; set; }
        public RangeBoundaryType MinBoundary { get; set; }
        public RangeBoundaryType MaxBoundary { get; set; }

        public Range()
        {

        }
        public Range(T min, T max) :
            this(min, RangeBoundaryType.Inclusive,
                max, RangeBoundaryType.Inclusive)
        {
        }
        public Range(T? min, T? max) :
            this(min.HasValue ? min.Value : default(T), RangeBoundaryType.Inclusive,
                max.HasValue ? max.Value : default(T), RangeBoundaryType.Inclusive)
        {
        }
        public Range(T min, RangeBoundaryType minBoundary,
            T max, RangeBoundaryType maxBoundary)
        {
            this.Min = min;
            this.Max = max;
            this.MinBoundary = minBoundary;
            this.MaxBoundary = maxBoundary;
        }

        //public bool Contains(Range<T> other)
        //{
        //    // TODO
        //}

        //public bool OverlapsWith(Range<T> other)
        //{
        //    // TODO
        //}

        public override string ToString()
        {
            return string.Format("{0} - {1}", Min, Max);
        }

        public override int GetHashCode()
        {
            return this.Min.GetHashCode() << 256 ^ this.Max.GetHashCode();
        }

        public bool Equals(Range<T> other)
        {
            if (other == null)
                return false;

            return
                this.Min.CompareTo(other.Min) == 0 &&
                this.Max.CompareTo(other.Max) == 0 &&
                this.MinBoundary == other.MinBoundary &&
                this.MaxBoundary == other.MaxBoundary;
        }

        public override bool Equals(object obj)
        {
            var other = obj as Range<T>;
            if (other == null)
                return false;

            return Equals(other);
        }

        public static bool operator ==(Range<T> left, Range<T> right)
        {
            if (!object.Equals(left, null))
                return left.Equals(right);
            else
                return object.Equals(right, null);
        }

        public static bool operator !=(Range<T> left, Range<T> right)
        {
            if (!object.Equals(left, null))
                return !left.Equals(right);
            else
                return !object.Equals(right, null);
        }

        public int CompareTo(Range<T> other)
        {
            if (this.Min.CompareTo(other.Min) != 0)
            {
                return this.Min.CompareTo(other.Min);
            }

            if (this.Max.CompareTo(other.Max) != 0)
            {
                this.Max.CompareTo(other.Max);
            }

            if (this.MinBoundary != other.MinBoundary)
            {
                return this.MinBoundary.CompareTo(other.Min);
            }

            if (this.MaxBoundary != other.MaxBoundary)
            {
                return this.MaxBoundary.CompareTo(other.MaxBoundary);
            }

            return 0;
        }
    }
}
