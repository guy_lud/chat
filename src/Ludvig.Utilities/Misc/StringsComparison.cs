﻿using System;

namespace Ludvig.Utilities.Misc
{
	public static class StringsComparison
	{
		public static bool EqualsIgnoreCase(this string target, string value)
		{
			return string.Equals(target, value, StringComparison.OrdinalIgnoreCase);
		}
	}
}