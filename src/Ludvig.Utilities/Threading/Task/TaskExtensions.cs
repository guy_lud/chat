﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ludvig.Utilities.Defence;

namespace Ludvig.Utilities.Threading.Task
{
	public static class TaskExtensions
	{

		public static Task<List<TResult>> ContinueOnCollection<T, TResult>(this Task<List<T>> task, Func<List<T>, List<TResult>> action)
		{
			var conTask = task.ContinueWith(x => x.Result.IsNullOrEmpty() ? new List<TResult>() : action(x.Result));

			return conTask;
		}

		public static Task<IEnumerable<TResult>> ContinueOnCollection<T, TResult>(this Task<IEnumerable<T>> task, Func<IEnumerable<T>, IEnumerable<TResult>> action)
		{
			var conTask = task.ContinueWith(x => x.Result.IsNullOrEmpty() ? Enumerable.Empty<TResult>() : action(x.Result));

			return conTask;
		}

		public static Task<IEnumerable<TResult>> ContinueOnCollection<T, TResult>(this Task<T[]> task, Func<T[], TResult[]> action)
		{
			var conTask = task.ContinueWith(x => x.Result.IsNullOrEmpty() ? Enumerable.Empty<TResult>() : action(x.Result));

			return conTask;
		}

		public static Task<ICollection<TResult>> ContinueOnCollection<T, TResult>(this Task<ICollection<T>> task, Func<ICollection<T>, ICollection<TResult>> action)
		{
			var conTask = task.ContinueWith(x => x.Result.IsNullOrEmpty() ? ((ICollection<TResult>) new List<T>()): action(x.Result));

			return conTask;
		}

		public static Task<IList<TResult>> ContinueOnCollection<T, TResult>(this Task<IList<T>> task, Func<IList<T>, IList<TResult>> action)
		{
			var conTask = task.ContinueWith(x => x.Result.IsNullOrEmpty() ? ((IList<TResult>)new List<T>()) : action(x.Result));

			return conTask;
		}
	}
}
