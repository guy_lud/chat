﻿using System;
using System.Threading;

namespace Ludvig.Utilities.Threading
{
	public static class Locker
	{
		public static T Lock<T>(object lockObject, Func<T> action, TimeSpan timeout)
		{
			if (!Monitor.TryEnter(lockObject, timeout))
				throw new TimeoutException($"Cannot acquire lock {lockObject}");

			try
			{
				return action();
			}
			finally
			{
				Monitor.Exit(lockObject);
			}
		}
	}
}
