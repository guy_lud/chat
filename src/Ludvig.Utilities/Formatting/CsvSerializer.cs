﻿using System;
using System.Text.RegularExpressions;

namespace Ludvig.Utilities.Formatting
{
	public interface ICsvSerializer
	{
		string ToCsvLine(params object[] values);

		string[] ToValues(string csvLine);
	}

	public class CsvSerializer : ICsvSerializer
	{
		private static readonly Regex SplittingRegex = new Regex(@",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))");
		private const string Quote = "\"";
		private const string EscapedQuote = "\"\"";
		private static readonly char[] CharactersThatMustBeQuoted = { ',', '"', '\r', '\n' };

		public string ToCsvLine(params object[] values)
		{
			if (values == null || values.Length == 0)
				return String.Empty;

			var stringValues = new string[values.Length];
			for (var i = 0; i < values.Length; i++)
			{
				stringValues[i] = Escape(String.Concat(values[i]));
			}

			return String.Join(",", stringValues);
		}

		public string[] ToValues(string csvLine)
		{
			var splitLine = SplittingRegex.Split(csvLine);
			var result = new string[splitLine.Length];
			for (var i = 0; i < result.Length; i++)
			{
				result[i] = splitLine[i].Trim('"').Replace(EscapedQuote, Quote);
			}
			return result;
		}

		private static string Escape(string text)
		{
			text = text.Replace(Quote, EscapedQuote);

			//Escape only if needed
			if (text.IndexOfAny(CharactersThatMustBeQuoted) > -1)
				text = Quote + text + Quote;

			return text;
		}
	}
}