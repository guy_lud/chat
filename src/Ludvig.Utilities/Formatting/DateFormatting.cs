﻿namespace Ludvig.Utilities.Formatting
{
	public static class DateFormattingExtensions
	{
		public static string ToFriendlyDateString(this System.DateTime date)
		{
			return date.ToString("MM/dd/yy");
		}

		public static string ToFriendlyDateTimeString(this System.DateTime date)
		{
			return date.ToString("MM/dd/yy h:mm tt");
		}

        public static string ToDB(this System.DateTime date)
        {
            return date.ToString("o");
        }
    }
}