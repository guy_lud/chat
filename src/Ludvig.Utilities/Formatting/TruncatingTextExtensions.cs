﻿using System;

namespace Ludvig.Utilities.Formatting
{
	public static class TruncatingTextExtensions
	{
		private const int DefaultMaxChars = 140;
		private const string DefaultSuffix = "...";

		public static string Truncate(this string text, int maxChars = DefaultMaxChars, string suffix = DefaultSuffix)
		{
			if (suffix != null && maxChars <= suffix.Length)
				throw new ArgumentException("maxChars must be bigger than suffix length");

			if (text == null || text.Length <= maxChars)
				return text;

			var index = maxChars - suffix?.Length ?? maxChars;
			return text.Remove(index) + suffix;
		}

		public static string TruncateWords(this string text, int maxChars = DefaultMaxChars, string suffix = DefaultSuffix, int maxWordLength = 20, char wordsSeparator = ' ')
		{
			if (suffix != null && maxChars <= suffix.Length)
				throw new ArgumentException("maxChars must be bigger than suffix length");

			if (text == null || text.Length <= maxChars)
				return text;

			suffix = suffix ?? "";
			
			var truncated = text.Truncate(maxChars - suffix.Length, null);
			var lastIndex = truncated.LastIndexOf(wordsSeparator);
			if (lastIndex == -1 || maxChars - lastIndex > maxWordLength)
				return truncated + suffix;

			return truncated.Remove(lastIndex) + suffix;
		}
	}
}