﻿using System;
using System.Linq;

namespace Ludvig.Utilities.Formatting
{
	public static class CasingExtensions
	{
		// Convert the string to Pascal case.
		public static string ToPascalCase(this string target)
		{
			// If there are 0 or 1 characters, just return the string.
			if (target == null) return null;
			if (target.Length < 2) return target.ToUpper();

			// Split the string into words.
			var words = target.Split(new char[] { }, StringSplitOptions.RemoveEmptyEntries);

			// Combine the words.

			return words.Aggregate("", (current, word) => current + word.Substring(0, 1).ToUpper() + word.Substring(1));
		}

		// Convert the string to camel case.
		public static string ToCamelCase(this string target)
		{
			// If there are 0 or 1 characters, just return the string.
			if (target == null || target.Length < 2)
				return target;

			// Split the string into words.
			var words = target.Split(new char[] { }, StringSplitOptions.RemoveEmptyEntries);

			// Combine the words.
			var result = words[0].ToLower();
			for (var i = 1; i < words.Length; i++)
			{
				result +=
					words[i].Substring(0, 1).ToUpper() +
					words[i].Substring(1);
			}

			return result;
		}

		// Capitalize the first character and add a space before
		// each capitalized letter (except the first character).
		public static string ToProperCase(this string target)
		{
			// If there are 0 or 1 characters, just return the string.
			if (target == null) return null;
			if (target.Length < 2) return target.ToUpper();

			// Start with the first character.
			var result = target.Substring(0, 1).ToUpper();

			// Add the remaining characters.
			for (var i = 1; i < target.Length; i++)
			{
				if (char.IsUpper(target[i])) result += " ";
				result += target[i];
			}

			return result;
		}

		public static string FromPascalTocamlCase(this string target)
		{
			if (target == null) return null;
			if (target.Length < 2) return target.ToLower();

			return target.Substring(0, 1).ToLower() + target.Substring(1);
		}
	}
}