﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Ludvig.Utilities.Formatting
{
	public static class JsonSerializationExtensions
	{
		public static string ToJsonString(this object target)
		{
			var settings = new JsonSerializerSettings
			{
				ContractResolver = new CamelCasePropertyNamesContractResolver()
			};

			return JsonConvert.SerializeObject(target, Newtonsoft.Json.Formatting.None, settings);
		}
	}
}