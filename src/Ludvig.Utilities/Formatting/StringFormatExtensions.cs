﻿using System.Text.RegularExpressions;

namespace Ludvig.Utilities.Formatting
{
	public static class StringFormatExtensions
	{
		public static string With(this string target, params object[] args)
		{
			return string.Format(target, args);
		}

		public static string[] SplitToLines(this string target)
		{
			return Regex.Split(target, "\r\n|\r|\n");
		}

		public static string Remove(this string target, string text)
		{
			return target.Replace(text, string.Empty);
		}

		public static string ChopTail(this string target, int count)
		{
			if (count >= target.Length)
				return string.Empty;

			return target.Remove(target.Length - count);
		}
	}
}