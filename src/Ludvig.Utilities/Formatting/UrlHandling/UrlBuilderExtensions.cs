﻿using System.Collections.Generic;
using Ludvig.Utilities.Defence;

namespace Ludvig.Utilities.Formatting.UrlHandling
{
	public static class UrlBuilderExtensions
	{
		public static UrlBuilder ToUrlBuilder(this string url)
		{
			return new UrlBuilder(url);
		}

		public static UrlBuilder WithQueryParam(this UrlBuilder urlBuilder, string key, string value)
		{
			urlBuilder.Query.AddOrUpdate(key, value);
			return urlBuilder;
		}

		public static UrlBuilder WithQueryParam<T>(this UrlBuilder urlBuilder, string key, T value)
		{
			urlBuilder.Query.AddOrUpdate(key, value?.ToString());
			return urlBuilder;
		}


		public static UrlBuilder WithCollectionQueryParam(this UrlBuilder urlBuilder, string key, ICollection<string> value)
		{
			urlBuilder.Query.AddOrUpdateCollection(key, value);
			return urlBuilder;
		}

		public static UrlBuilder Secured(this UrlBuilder urlBuilder)
		{
			urlBuilder.Protocol = "https";
			return urlBuilder;
		}

		public static UrlBuilder WithQueryParamsFromAnotherUrl(this UrlBuilder target, string url)
		{
			if (url.IsNullOrEmpty())
			{
				return target;
			}

			var otherUrlBuilder = new UrlBuilder(url);
			foreach (string key in otherUrlBuilder.Query.Values.Keys)
			{
				var value = otherUrlBuilder.Query.GetValue(key);
				target.Query.AddOrUpdate(key, value);
			}

			return target;
		}
	}
}
