﻿using System;

namespace Ludvig.Modules.Contracts.Entities
{
	public interface ICollectionlessEntity
	{
		
	}

	public interface IEntity<TKey>
	{
		TKey Id { get; set; }
	}

	public static class EntityExtensions
	{
		public static string GetIdPart(this IEntity<string> target, int part)
		{
			var parts = target.Id.Split(new[] {"::"}, StringSplitOptions.RemoveEmptyEntries);
			if (parts.Length > part)
				return parts[part];

			return "";
		}
	}
}
