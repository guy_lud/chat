﻿namespace Ludvig.Modules.Contracts.Reports
{
	public interface IReportRepository
	{
		Report<T> GetReport<T>(string type) where T : class, new();
		void SaveReport<T>(string type, T data);
	}

	public static class ReportRepositoryExtensions
	{
		public static void SaveReport<T>(this IReportRepository target,T data)
		{
			target.SaveReport(typeof(T).Name, data);
		}

		public static Report<T> GetReport<T>(this IReportRepository target) where T : class, new()
		{
			return target.GetReport<T>(typeof(T).Name);
		}
	}
}
