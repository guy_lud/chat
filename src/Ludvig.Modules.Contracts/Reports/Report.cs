﻿using System;

namespace Ludvig.Modules.Contracts.Reports
{
	public class Report<T> where T :class, new()
	{
		public string Type { get; set; }
		public T Data { get; set; }
		public DateTime TimeCreated { get; set; }
	}
}
