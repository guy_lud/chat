﻿using System.Collections.Generic;

namespace Ludvig.Modules.Contracts.Reports
{
	public class MessageAvrageTimeReport
	{
		public long Avg { get; set; }
		public int LastCount { get; set; }
		public int LastId { get; set; }
	}

	public class MessagesAvrageLengthReport
	{
		public float Avg { get; set; }
		public int LastCount { get; set; }
		public int LastId { get; set; }
		public List<UserMessageWordsAvrage> UserMessageWordsAvragesCollection { get; set; }
	}

	public class UserMessageWordsAvrage
	{
		public string UserId { get; set; }
		public int LastCount { get; set; }
		public float LastAvrage { get; set; }
	}
}
