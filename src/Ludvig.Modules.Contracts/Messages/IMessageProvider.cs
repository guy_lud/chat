﻿using System.Collections.Generic;

namespace Ludvig.Modules.Contracts.Messages
{
	public interface IMessageProvider
	{
		IEnumerable<Message> FetchAllMessages();
		IEnumerable<Message> FetchDeltaMessages(int lastId);
	}
}
