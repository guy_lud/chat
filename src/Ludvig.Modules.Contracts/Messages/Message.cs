﻿using System;

namespace Ludvig.Modules.Contracts.Messages
{
	public class Message
	{
		public int Id { get; set; }
		public string SenderId { get; set; }
		public string Content { get; set; }
		public DateTime TimeCreated { get; set; }
	}
}
