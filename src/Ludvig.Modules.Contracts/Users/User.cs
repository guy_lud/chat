
namespace Ludvig.Modules.Contracts.Users
{
	public class User
	{
		public UserId Id { get; set; }
		public string UserName { get; set; }
	}
}