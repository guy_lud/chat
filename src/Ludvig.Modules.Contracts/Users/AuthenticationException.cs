using System;

namespace Ludvig.Modules.Contracts.Users
{
	public class AuthenticationException : ApplicationException
	{
		public AuthenticationException()
		{
		}

		public AuthenticationException(string message) : base(message)
		{
		}
	}

	public class UserAlreadyExistsAuthenticationException : AuthenticationException
	{

	}

	public class UserDoesntExistsAuthenticationException : AuthenticationException
	{

	}

	public class IncorrectPasswordAuthenticationException : AuthenticationException
	{

	}
}