using Ludvig.Utilities.Caching;

namespace Ludvig.Modules.Contracts.Users
{
	public class CurrentUserProvider : ICurrentUserProvider
	{
		private const string CacheKey = "LoggedInUser";
		
		private readonly IRequestLevelCacher _requestLevelCacher;

		public CurrentUserProvider(IRequestLevelCacher requestLevelCacher)
		{
			_requestLevelCacher = requestLevelCacher;
		}

		public User GetCurrentUser()
		{
			return _requestLevelCacher.Get<User>(CacheKey);
		}
	}
}