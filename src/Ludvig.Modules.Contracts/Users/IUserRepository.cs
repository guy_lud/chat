﻿namespace Ludvig.Modules.Contracts.Users
{
	public interface IUserRepository
	{
		User GetUserByName(string userName);
		User GetUserById(string id);
		void UpdateUser(User user);
	}
}
