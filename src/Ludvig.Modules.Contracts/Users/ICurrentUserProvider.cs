using System;

namespace Ludvig.Modules.Contracts.Users
{
	public interface ICurrentUserProvider
	{
		User GetCurrentUser();
	}

	public static class CurrentUserProviderExtensions
	{
		public static bool IsLoggedIn(this ICurrentUserProvider target)
		{
			return target.GetCurrentUser() != null;
		}

		public static string GetCurrentUserId(this ICurrentUserProvider target)
		{
			if (!target.IsLoggedIn())
				throw new UnauthorizedAccessException();

			return target.GetCurrentUser().Id;
		}
	}
}