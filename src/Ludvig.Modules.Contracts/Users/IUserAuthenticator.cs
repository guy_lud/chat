namespace Ludvig.Modules.Contracts.Users
{
	public interface IUserAuthenticator
	{
		string Login(string userName);
		User GetSessionUser(string sessionId);
	}
}