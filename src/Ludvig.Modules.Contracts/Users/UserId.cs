﻿using System;

namespace Ludvig.Modules.Contracts.Users
{
	public class UserId
	{
		private readonly string _userId;

		public UserId(string userId)
		{
			if (userId == null)
				throw new ArgumentNullException(nameof(userId));
			_userId = userId;
		}

		public override bool Equals(object obj)
		{
			return Equals((UserId)obj);
		}

		protected bool Equals(UserId other)
		{
			return string.Equals(_userId, other._userId);
		}

		public override int GetHashCode()
		{
			return _userId?.GetHashCode() ?? 0;
		}

		public static implicit operator string(UserId userId)
		{
			return userId._userId;
		}

		public static implicit operator UserId(string userId)
		{
			return new UserId(userId);
		}

		public override string ToString()
		{
			return _userId;
		}
	}
}
