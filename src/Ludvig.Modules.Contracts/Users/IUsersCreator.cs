namespace Ludvig.Modules.Contracts.Users
{
	public interface IUsersCreator
	{
		void SignUp(string userName);
	}
}