using System;
using Ludvig.Utilities.Ioc;
using SimpleInjector;

namespace Ludvig.Infrastructure.Bootstrap
{
	class SimpleInjectorServiceRegistrator : IServiceRegistrator
	{
		private readonly Container _container;

		public SimpleInjectorServiceRegistrator(Container container)
		{
			_container = container;
		}

		public IServiceRegistrator Register(Type type, Type implementationType)
		{
			_container.Register(type, implementationType, Lifestyle.Singleton);
			return this;
		}

		public IServiceRegistrator Register(Type type, object instance)
		{
			_container.Register(type, () => instance, Lifestyle.Singleton);
			return this;
		}
	}
}