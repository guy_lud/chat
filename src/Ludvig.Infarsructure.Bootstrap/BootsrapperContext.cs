using System;
using System.Collections.Generic;
using System.Reflection;
using SimpleInjector;

namespace Ludvig.Infrastructure.Bootstrap
{
	public class BootsrapperContext
	{
		public BootsrapperContext(Container container, Type[] typeCollection, Assembly[] assemblies)
		{
			Container = container;
			TypeCollection = typeCollection;
			Assemblies = assemblies;
		}

		public Container Container { get; }
		public Type[] TypeCollection { get; }
		public IEnumerable<Assembly> Assemblies { get; }
	}
}