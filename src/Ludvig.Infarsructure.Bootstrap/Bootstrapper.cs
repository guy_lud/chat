using System;
using System.Collections.Generic;
using System.Linq;
using Ludvig.Utilities.Collections;
using Ludvig.Utilities.Ioc;
using Ludvig.Utilities.Reflection;
using SimpleInjector;

namespace Ludvig.Infrastructure.Bootstrap
{
	public class Bootstrapper : IBootsrapper
	{
		private readonly Container _container;


		public Bootstrapper(Container container)
		{
			_container = container ?? new Container();
		}

		public IServiceResolver Initialize(AssemblyCollection assemblies, ModuleCollection modules)
		{
			var loader = new AssemblyLoader(assemblies);

			_container.Register<IAssemblyLoader>(() => loader);

			RegisterAllServices(loader.Types);

			var context = new BootsrapperContext(_container, loader.Types.ToArray(), loader.Assemblies.ToArray());

			modules.ForEach(x => x.OnBootstrap(context));

			return new SimpleInjectorServiceResolver(_container);
		}

		private void RegisterAllServices(IEnumerable<Type> types)
		{
			var enumerable = types as Type[] ?? types.ToArray();

			enumerable.Where(t => !t.IsGenericType && !t.IsAbstract)
				.Select(t => new { Type = t, DefaultInterface = t.GetInterface("I" + t.Name) })
				.Where(t => t.DefaultInterface != null && !t.DefaultInterface.HasCustomAttribute<SkipAutoRegistrationAttribute>())
				.ForEach(t => _container.Register((Type)t.DefaultInterface, (Type)t.Type));

			enumerable.Where(t => !t.IsGenericType && !t.IsAbstract && t.GetInterfaces().Any(i => i.HasCustomAttribute<RegisterAllAttribute>())).ToArray()
				.GroupBy(x => x.GetInterfaces().First(i => i.HasCustomAttribute<RegisterAllAttribute>()))
				.ForEach(col => _container.RegisterCollection(col.Key, col));

			enumerable.Where(t => !t.IsGenericType && !t.IsAbstract && t.BaseType.HasCustomAttribute<RegisterAllAttribute>()).ToArray()
				.GroupBy(x => x.BaseType)
				.ForEach(col => _container.RegisterCollection(col.Key, col));
		}
	}
}