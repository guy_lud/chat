namespace Ludvig.Infrastructure.Bootstrap
{
	public interface IBootstrapModule
	{
		void OnBootstrap(BootsrapperContext context);
	}

	public interface IAppBuilderModule
	{
		void OnBuild(AppBuilderContext context);
	}
}