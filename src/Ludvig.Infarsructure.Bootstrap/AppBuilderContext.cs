using Ludvig.Utilities.Configuration;
using Ludvig.Utilities.Ioc;

namespace Ludvig.Infrastructure.Bootstrap
{
	public class AppBuilderContext
	{
		public IServiceResolver Resolver { get; }
		public IHostingEnvironment HostingEnvironment { get; }

		public AppBuilderContext(IServiceResolver resolver, IHostingEnvironment hostingEnvironment)
		{
			Resolver = resolver;
			HostingEnvironment = hostingEnvironment;
		}
	}
}