﻿using Ludvig.Infrastructure.Bootstrap.Logging;
using Ludvig.Infrastructure.Bootstrap.Settings;
using Ludvig.Utilities.Configuration;

namespace Ludvig.Infrastructure.Bootstrap
{
	public static class BootstrapperCommonExtensions
	{
		public static ModuleCollection AddBasicModules(this ModuleCollection target,
			IHostingEnvironment hostingEnvironment)
		{
			return target
				.AddWarehouse(hostingEnvironment)
				.AddNLog()
				.AddSettings();
		}

		public static IApplicationBuilder UseBasicModules(this IApplicationBuilder target)
		{
			target.AddModule(new SettingsValuesReaderModule());
			return target.UseNLog(x => x.UseFileTarget());
		}
	}
}