using System;
using System.Collections.Generic;
using System.Linq;
using Ludvig.Utilities.Configuration;

namespace Ludvig.Infrastructure.Bootstrap.Settings
{
	public interface ISettingsValuesReader
	{
		void SetSettingsValues(IEnumerable<ISettingsImplementor> allSettings);
	}

	public class SettingsValuesReader : ISettingsValuesReader
	{
		private const string SettingsPrefix = "Settings:";
		private readonly IConfigurationManager _configurationManager;

		public SettingsValuesReader(IConfigurationManager configurationManager)
		{
			_configurationManager = configurationManager;
		}

		public void SetSettingsValues(IEnumerable<ISettingsImplementor> allSettings)
		{
			var appSettings = _configurationManager.AppSettings.AllKeys
				.Where(x => x.StartsWith(SettingsPrefix,StringComparison.CurrentCultureIgnoreCase));

			var data = appSettings.ToDictionary(x => x.Replace(SettingsPrefix, string.Empty).Replace(":",".") , x => (object) _configurationManager.AppSettings[x]);

			foreach (var settings in allSettings)
				settings.SetValues(data);
		}
	}
}