using System;
using System.Linq;

namespace Ludvig.Infrastructure.Bootstrap.Settings
{
	public class SettingsValuesReaderModule : IAppBuilderModule
	{
		public void OnBuild(AppBuilderContext context)
		{
			var allSettings = context.Resolver.FindRegisteredInstances(x => x.Name.EndsWith("Settings")).OfType<ISettingsImplementor>().ToList();
			if (!allSettings.Any())
				return;
			var reader = context.Resolver.Resolve<ISettingsValuesReader>();
			reader.SetSettingsValues(allSettings);
		}
	}
}