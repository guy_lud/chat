using System;
using System.IO;
using System.Linq;
using Ludvig.DataAccess.Databases;
using Ludvig.DataAccess.Databases.SqlLite;
using Ludvig.Utilities.Configuration;
using SimpleInjector;

namespace Ludvig.Infrastructure.Bootstrap.Settings
{
	public class WarehouseBootstrapModule : IBootstrapModule
	{
		private readonly IHostingEnvironment _hostingEnvironment;

		public WarehouseBootstrapModule(IHostingEnvironment hostingEnvironment)
		{
			_hostingEnvironment = hostingEnvironment;
		}

		public void OnBootstrap(BootsrapperContext context)
		{
			var databaseFilePath = Path.Combine(_hostingEnvironment.ApplicationRootPath, _hostingEnvironment.SqliteDatabasePath());

			context.Container.RegisterSingleton<ISqlConnectionProvider>(() => new SqlLiteConnectionProvider(databaseFilePath));
			context.Container.RegisterSingleton(typeof(IWarehouse<>), typeof(SqlLiteWarehouse<>));
			//context.Container.RegisterSingleton(typeof(IWarehouse<SessionDto>), typeof(SqlLiteWarehouse<SessionDto>));
			//context.Container.RegisterSingleton(typeof(IWarehouse<SessionDto>), typeof(SqlLiteWarehouse<SessionDto>));
			//IWarehouse<SettingsNodeDto>

		}
	}

	public class SettingsBootstrapModule : IBootstrapModule
	{
		public void OnBootstrap(BootsrapperContext context)
		{
			var settingsTypes = context.TypeCollection.Where(x => x.IsInterface && x.Name.EndsWith("Settings"));

			foreach (var settingsType in settingsTypes)
			{
				var instance = (ISettingsImplementor)Activator.CreateInstance(SettingsImplementorGenerator.CreateClass(settingsType));
				instance.SetValues();

				context.Container.Register(settingsType, () => instance, Lifestyle.Singleton);
			}
		}
	}

	public static class CommonBootstrapperExtensions
	{
		public static ModuleCollection AddSettings(this ModuleCollection target)
		{
			target.Add(new SettingsBootstrapModule());
			return target;
		}

		public static ModuleCollection AddWarehouse(this ModuleCollection target, IHostingEnvironment hostingEnvironment)
		{
			target.Add(new WarehouseBootstrapModule(hostingEnvironment));
			return target;
		}
	}
}