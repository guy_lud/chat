using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Ludvig.Infrastructure.Bootstrap.Settings
{
	public static class SettingsImplementorGenerator
	{
		private static readonly ModuleBuilder _moduleBuilder;

		static SettingsImplementorGenerator()
		{
			var asmName = new AssemblyName("SettingsImplementations");
			var asmBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(asmName, AssemblyBuilderAccess.Run);
			_moduleBuilder = asmBuilder.DefineDynamicModule("core");
		}

		public static Type CreateClass(Type interfaceType)
		{
			var name = "SettingsImplementations." + interfaceType.Namespace + "." + interfaceType.Name.Substring(1) + "Impl";

			var existingType = _moduleBuilder.Assembly.GetType(name.Replace("+", "\\+"));
			if (existingType != null)
				return existingType;

			var baseType = typeof(SettingsImplementor<>).MakeGenericType(interfaceType);

			try
			{
				var typeBuilder = _moduleBuilder.DefineType(name, TypeAttributes.Public | TypeAttributes.Class);
				typeBuilder.AddInterfaceImplementation(interfaceType);
				typeBuilder.SetParent(baseType);

				foreach (var propertyInfo in GetInterfaceProperties(interfaceType))
					CreateProperty(typeBuilder, propertyInfo);

				var type = typeBuilder.CreateType();

				return type;
			}
			catch (Exception e)
			{
				throw new Exception("Can't create Implementation for: " + interfaceType.FullName, e);
			}


		}

		private static void CreateProperty(TypeBuilder builder, PropertyInfo propertyInfo)
		{
			var name = propertyInfo.Name;
			var type = propertyInfo.PropertyType;

			var fieldBuilder = builder.DefineField("_" + name, type, FieldAttributes.Private);

			var propertyBuilder = builder.DefineProperty(name, PropertyAttributes.HasDefault, propertyInfo.PropertyType, null);
			const MethodAttributes attributes = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Final | MethodAttributes.Virtual;

			var getterBuilder = builder.DefineMethod("get_" + name, attributes, type, Type.EmptyTypes);
			var getter = getterBuilder.GetILGenerator();
			getter.Emit(OpCodes.Ldarg_0);
			getter.Emit(OpCodes.Ldfld, fieldBuilder);
			getter.Emit(OpCodes.Ret);

			var setterBuilder = builder.DefineMethod("set_" + name, attributes, null, new[] { type });
			var setter = setterBuilder.GetILGenerator();
			setter.Emit(OpCodes.Ldarg_0);
			setter.Emit(OpCodes.Ldarg_1);
			setter.Emit(OpCodes.Stfld, fieldBuilder);
			setter.Emit(OpCodes.Ret);

			propertyBuilder.SetGetMethod(getterBuilder);
			propertyBuilder.SetSetMethod(setterBuilder);
		}

		internal static IEnumerable<PropertyInfo> GetInterfaceProperties(Type type)
		{
			var properties = type.GetProperties().ToList();
			var inherited = type
				.GetInterfaces()
				.SelectMany(x => x.GetProperties())
				.ToList();

			foreach (var property in inherited.Where(p => properties.All(x => x.Name != p.Name)))
				properties.Add(property);

			return properties;
		}
	}
}