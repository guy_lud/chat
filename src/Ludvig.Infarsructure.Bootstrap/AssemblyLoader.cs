using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Ludvig.Infrastructure.Bootstrap
{
	internal class AssemblyLoader : IAssemblyLoader
	{
		private readonly Lazy<IEnumerable<Type>> _types;

		public IEnumerable<Assembly> Assemblies { get; }

		public IEnumerable<Type> Types => _types.Value;

		public AssemblyLoader(AssemblyCollection assemblies)
		{
			Assemblies = assemblies;
			_types = new Lazy<IEnumerable<Type>>(GetAllTypes);
		}

		private IEnumerable<Type> GetAllTypes()
		{
			return Assemblies.Distinct().SelectMany(x => x.GetTypes()).ToArray();
		}
	}
}