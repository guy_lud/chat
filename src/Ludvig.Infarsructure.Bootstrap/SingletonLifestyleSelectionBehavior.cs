using System;
using SimpleInjector;
using SimpleInjector.Advanced;

namespace Ludvig.Infrastructure.Bootstrap
{
	public class SingletonLifestyleSelectionBehavior : ILifestyleSelectionBehavior
	{
		public Lifestyle SelectLifestyle(Type serviceType, Type implementationType)
		{
			return Lifestyle.Singleton;
		}
	}
}