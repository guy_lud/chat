using System;
using NLog.Config;

namespace Ludvig.Infrastructure.Bootstrap.Logging
{
	public class LoggingConfigurationBuilder : ILoggingConfigurationBuilder
	{
		private readonly NLogAppBuilderModule _nLogAppBuilderModule;

		public LoggingConfigurationBuilder(NLogAppBuilderModule nLogAppBuilderModule)
		{
			_nLogAppBuilderModule = nLogAppBuilderModule;
		}

		public ILoggingConfigurationBuilder With(Action<LoggingConfiguration, AppBuilderContext> action)
		{
			_nLogAppBuilderModule.Configure(action);
			return this;
		}
	}
}