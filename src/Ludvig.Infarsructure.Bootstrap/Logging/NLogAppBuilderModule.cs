﻿using System;
using System.Collections.Generic;
using Ludvig.Infrastructure.Bootstrap.System;
using Ludvig.Utilities.Logging;
using NLog;
using NLog.Config;
using SimpleInjector;
using ILogger = Ludvig.Utilities.Logging.ILogger;

namespace Ludvig.Infrastructure.Bootstrap.Logging
{
	public class NLogAppBuilderModule : IBootstrapModule, IAppBuilderModule
	{
		private readonly List<Action<LoggingConfiguration, AppBuilderContext>> _configurationActions = new List<Action<LoggingConfiguration, AppBuilderContext>>();

		public NLogAppBuilderModule Configure(Action<LoggingConfiguration, AppBuilderContext> action)
		{
			_configurationActions.Add(action);
			return this;
		}

		public void OnBootstrap(BootsrapperContext context)
		{
			var loggerFactory = new LoggerFactory();
			loggerFactory.AddProvider(new NLogLoggerProvider());

			context.Container.Register<ILoggerFactory>(() => loggerFactory, Lifestyle.Singleton);

			context.Container.RegisterConditional(typeof(ILogger),
				f =>
				{
					if (f.Consumer == null)
						throw new RegistrationViolationException("Unable to resolve logger, probably you've requested it directly and not via c'tor." +
							"to use logger outside c'tor scope use IFactoryLogger");

					return typeof(Logger<>).MakeGenericType(f.Consumer.ImplementationType);
				},
				Lifestyle.Singleton,
				c => true);
		}

		public void OnBuild(AppBuilderContext context)
		{
			var config = new LoggingConfiguration();

			_configurationActions.ForEach(x => x(config, context));

			LogManager.Configuration = config;
		}
	}
}
