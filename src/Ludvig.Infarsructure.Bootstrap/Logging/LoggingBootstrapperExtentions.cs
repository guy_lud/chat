using System;

namespace Ludvig.Infrastructure.Bootstrap.Logging
{
	public static class LoggingBootstrapperExtentions
	{
		public static ModuleCollection AddNLog(this ModuleCollection target)
		{
			var module = new NLogAppBuilderModule();
			target.Add(module);
			return target;
		}

		public static IApplicationBuilder UseNLog(this IApplicationBuilder target, Action<ILoggingConfigurationBuilder> buildAction)
		{
			var module = new NLogAppBuilderModule();
			var builder = new LoggingConfigurationBuilder(module);
			buildAction(builder);
			target. AddModule(module);
			return target;
		}
	}
}