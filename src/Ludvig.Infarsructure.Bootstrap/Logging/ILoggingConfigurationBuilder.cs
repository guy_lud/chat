using System;
using Ludvig.Utilities.Ioc;
using NLog.Config;

namespace Ludvig.Infrastructure.Bootstrap.Logging
{
	[SkipAutoRegistration]
	public interface ILoggingConfigurationBuilder
	{
		ILoggingConfigurationBuilder With(Action<LoggingConfiguration, AppBuilderContext> action);
	}
}