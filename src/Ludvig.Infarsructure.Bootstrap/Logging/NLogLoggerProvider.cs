using System;
using System.Globalization;
using Ludvig.Utilities.Logging;
using NLog;
using ILogger = Ludvig.Utilities.Logging.ILogger;
using LogLevel = Ludvig.Utilities.Logging.LogLevel;

namespace Ludvig.Infrastructure.Bootstrap.Logging
{
	public class NLogLoggerProvider : ILoggerProvider
	{
		private bool _disposed = false;

		public ILogger CreateLogger(string name)
		{
			return new Logger(LogManager.GetLogger(name));
		}

		public void Dispose()
		{
			if (!_disposed)
			{
				_disposed = true;
				LogManager.Flush();
			}
		}

		private class Logger : Utilities.Logging.ILogger
		{
			private readonly global::NLog.Logger _logger;

			public Logger(global::NLog.Logger logger)
			{
				_logger = logger;
			}

			public void Log(
				LogLevel logLevel,
				int eventId,
				object state,
				Exception exception,
				Func<object, Exception, string> formatter)
			{
				var nLogLogLevel = GetLogLevel(logLevel);
				var message = string.Empty;
				if (formatter != null)
				{
					message = formatter(state, exception);
				}
				else
				{
					message = LogFormatter.Formatter(state, exception);
				}
				if (!string.IsNullOrEmpty(message))
				{
					var eventInfo = LogEventInfo.Create(nLogLogLevel, _logger.Name, exception, CultureInfo.InvariantCulture, message);
					eventInfo.Properties["EventId"] = eventId;
					_logger.Log(eventInfo);
				}
			}

			public bool IsEnabled(LogLevel logLevel)
			{
				return _logger.IsEnabled(GetLogLevel(logLevel));
			}

			private global::NLog.LogLevel GetLogLevel(LogLevel logLevel)
			{
				switch (logLevel)
				{
					case LogLevel.Verbose: return global::NLog.LogLevel.Trace;
					case LogLevel.Debug: return global::NLog.LogLevel.Debug;
					case LogLevel.Information: return global::NLog.LogLevel.Info;
					case LogLevel.Warning: return global::NLog.LogLevel.Warn;
					case LogLevel.Error: return global::NLog.LogLevel.Error;
					case LogLevel.Critical: return global::NLog.LogLevel.Fatal;
				}
				return global::NLog.LogLevel.Debug;
			}

			public IDisposable BeginScopeImpl(object state)
			{
				if (state == null)
				{
					throw new ArgumentNullException(nameof(state));
				}

				return NestedDiagnosticsContext.Push(state.ToString());
			}
		}
	}
}