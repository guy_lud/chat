using NLog.Config;
using NLog.Targets;
using NLog.Targets.Wrappers;
using LogLevel = NLog.LogLevel;

namespace Ludvig.Infrastructure.Bootstrap.Logging
{
	public static class LoggingConfigurationBuilderExtentions
	{
		public static ILoggingConfigurationBuilder UseFileTarget(this ILoggingConfigurationBuilder target)
		{
			target.With((config, context) =>
			{
				var fileTarget = new FileTarget
				{
					Layout = @"${date:format=HH\:mm\:ss} ${logger} ${message}",
					FileName = "${basedir}/file.txt"
				};
				config.AddTarget("file_log", new AsyncTargetWrapper(fileTarget));


				var rule1 = new LoggingRule("*", LogLevel.Debug, fileTarget);
				config.LoggingRules.Add(rule1);
			});

			return target;
		}
	}
}