using SimpleInjector;

namespace Ludvig.Infrastructure.Bootstrap
{
	public static class SimpleInjectorExtensions
	{
		public static Container UseSingletonLifestyleBehavior(this Container container)
		{
			container.Options.LifestyleSelectionBehavior = new SingletonLifestyleSelectionBehavior();
			return container;
		}
	}
}