﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Ludvig.Utilities.Ioc;

namespace Ludvig.Infrastructure.Bootstrap
{
	[SkipAutoRegistration]
	internal interface IAssemblyLoader
	{
		IEnumerable<Assembly> Assemblies { get; }
		IEnumerable<Type> Types { get; }
	}
}