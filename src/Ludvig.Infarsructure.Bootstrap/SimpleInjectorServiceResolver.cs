using System;
using System.Collections.Generic;
using System.Linq;
using Ludvig.Utilities.Ioc;
using SimpleInjector;

namespace Ludvig.Infrastructure.Bootstrap
{
	public class SimpleInjectorServiceResolver : IServiceResolver
	{
		private readonly Container _container;

		public Container Container => _container;

		public SimpleInjectorServiceResolver(Container container)
		{
			_container = container;
		}

		public object Resolve(Type service)
		{
			return _container.GetInstance(service);
		}

		public object[] ResolveAll(Type service)
		{
			return _container.GetAllInstances(service).ToArray();
		}

		public T Resolve<T>() where T : class
		{
			return _container.GetInstance<T>();
		}
		
		public IEnumerable<T> ResolveAll<T>() where T : class
		{
			return _container.GetAllInstances<T>();
		}

		public IList<object> FindRegisteredInstances(Func<Type, bool> predicate)
		{
			return Container.GetCurrentRegistrations()
				.Where(x => predicate(x.Registration.ImplementationType))
				.Select(x => x.GetInstance())
				.ToList();
		}
	}
}