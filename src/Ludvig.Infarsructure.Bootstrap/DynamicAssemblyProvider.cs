using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Ludvig.Infrastructure.Bootstrap
{
	internal static class DynamicAssemblyProvider
	{
		private static Assembly[] _modules;

		public static Assembly[] GetModules(string binFolder)
		{
			if (_modules != null)
				return _modules;

			_modules = Directory
				.GetFiles(binFolder, "Ludvig.*.dll")
				.Select(x => LoadAssembly(x, binFolder))
				.ToArray();

			return _modules;
		}

		private static Assembly LoadAssembly(string assemblyName, string binFolder)
		{
			assemblyName = Path.GetFileNameWithoutExtension(assemblyName);

			var assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.GetName().Name == assemblyName);
			if (assembly != null)
				return assembly;

			return assemblyName == null ? null : Assembly.Load(assemblyName);
		}
	}
}