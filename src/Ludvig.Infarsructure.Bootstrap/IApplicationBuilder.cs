using System.Collections.Generic;
using Ludvig.Utilities.Configuration;
using Ludvig.Utilities.Ioc;

namespace Ludvig.Infrastructure.Bootstrap
{
	[SkipAutoRegistration]
	public interface IApplicationBuilder
	{
		IServiceResolver Resolver { get; }
		IHostingEnvironment HostingEnvironment { get; }

		void AddModule(IAppBuilderModule module);
		void Build();
	}

	public class ApplicationBuilder : IApplicationBuilder
	{
		private readonly List<IAppBuilderModule> _modules = new List<IAppBuilderModule>();

		public IServiceResolver Resolver { get; }
		public IHostingEnvironment HostingEnvironment { get; }

		public ApplicationBuilder(IServiceResolver resolver, IHostingEnvironment hostingEnvironment)
		{
			Resolver = resolver;
			HostingEnvironment = hostingEnvironment;
		}

		public void AddModule(IAppBuilderModule module)
		{
			_modules.Add(module);
		}

		public void Build()
		{
			var appBuilderContext = new AppBuilderContext(Resolver, HostingEnvironment);

			_modules.ForEach(x => x.OnBuild(appBuilderContext));
		}
	}
}