using System.Collections.Generic;
using System.Reflection;
using Ludvig.Utilities.Ioc;

namespace Ludvig.Infrastructure.Bootstrap
{
	[SkipAutoRegistration]
	public interface IBootsrapper
	{
		IServiceResolver Initialize(AssemblyCollection assemblies, ModuleCollection modules);
	}

	public class AssemblyCollection : List<Assembly>
	{
		public static AssemblyCollection New()
		{
			return new AssemblyCollection();
		}
	}

	public class ModuleCollection : List<IBootstrapModule>
	{
		public static ModuleCollection New()
		{
			return new ModuleCollection();
		}
	}

	public static class AssmeblyCollectionExtensions
	{
		public static AssemblyCollection AddEntryAssembly(this AssemblyCollection target)
		{
			target.Add(Assembly.GetEntryAssembly());

			return target;
		}

		public static AssemblyCollection AddFromType<T>(this AssemblyCollection target)
		{
			target.Add(typeof(T).Assembly);

			return target;
		}

		public static AssemblyCollection AddModules(this AssemblyCollection assemblies, string binFolderPath)
		{
			assemblies.AddRange(DynamicAssemblyProvider.GetModules(binFolderPath));
			return assemblies;
		}
	}
}