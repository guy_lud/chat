﻿namespace Ludvig.Infrastructure.Bootstrap.PreBuildToolRef
{
	public static class PreBuildReferencer
	{
		static PreBuildReferencer()
		{
			// ReSharper disable ReturnValueOfPureMethodIsNotUsed
			typeof(Modules.Messages.Referencer).ToString();
			typeof(Modules.Users.Referencer).ToString();
			typeof(Modules.Reports.Referencer).ToString();
			// ReSharper restore ReturnValueOfPureMethodIsNotUsed
		}
	}
}
