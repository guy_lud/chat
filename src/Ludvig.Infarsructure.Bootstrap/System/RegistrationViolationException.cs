using System;
using System.Runtime.Serialization;
using Ludvig.Utilities.Formatting;

namespace Ludvig.Infrastructure.Bootstrap.System
{
	[Serializable]
	public class RegistrationViolationException : Exception
	{
		private const string InnerMessage = "Something went wrong with the regstration or resloving process, you probably made registration mistake: [{0}]";

		public RegistrationViolationException(string message)
			: base(InnerMessage.With(message))
		{
		}

		public RegistrationViolationException(string message, Exception inner) :
			base(InnerMessage.With(message), inner)
		{
		}

		protected RegistrationViolationException(
			SerializationInfo info,
			StreamingContext context) : base(info, context)
		{
		}
	}
}