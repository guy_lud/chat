using System.Threading.Tasks;
using Ludvig.Utilities.Ioc;

namespace Ludvig.Infrastructure.Bootstrap.Warmup
{
	[RegisterAll]
	public interface IWarmupTask
	{
		Task Run();
	}
}