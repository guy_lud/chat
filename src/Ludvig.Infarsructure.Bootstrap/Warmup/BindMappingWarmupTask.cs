using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ludvig.Utilities.Mapping;

namespace Ludvig.Infrastructure.Bootstrap.Warmup
{
	public class BindMappingWarmupTask : IWarmupTask
	{
		private readonly IBindingMapper[] _bindingMappers;

		public BindMappingWarmupTask(IEnumerable<IBindingMapper> bindingMappers)
		{
			_bindingMappers = bindingMappers.ToArray();
		}

		public Task Run()
		{
			var context = new ExpressMapperBindingContex();

			foreach (var bindingMapper in _bindingMappers)
			{
				bindingMapper.Bind(context);
			}

			return Task.CompletedTask;
		}
	}
}