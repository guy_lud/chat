﻿using System;
using Ludvig.Infrastructure.Bootstrap;
using Ludvig.TaskRunner.Infra;
using Ludvig.Utilities.Configuration;
using Ludvig.Utilities.Defence;
using Ludvig.Utilities.Ioc;
using SimpleInjector;

namespace Ludvig.TaskRunner
{
	public class Program
	{
		private static IHostingEnvironment _hostingEnvironment;

		private static Container Container { get; } = new Container();

		static void Main(string[] args)
		{
			Bootstrap();

			if (args.IsNullOrEmpty())
			{
				var defaultTask = Container.GetInstance<IConfigurationManager>().AppSettings["TaskRunner:DefaultTask"];
				if (_hostingEnvironment.IsProduction() || defaultTask.IsNullOrEmpty())
				{
					Console.WriteLine("Please specify the task name");
					return;
				}

				args = new[] { defaultTask };
			}

			var initator = Container.GetInstance<ITaskInitiator>();

			initator.Run(args);
		}

		private static void Bootstrap()
		{
			Container.Options.LifestyleSelectionBehavior = new SingletonLifestyleSelectionBehavior();

			var configurationManager = new SystemConfigurationManager();

			_hostingEnvironment = new HostingEnvironment(configurationManager, AppDomain.CurrentDomain.BaseDirectory);

			Container.RegisterSingleton<IConfigurationManager>(() => configurationManager);
			Container.RegisterSingleton(typeof(IHostingEnvironment), () => _hostingEnvironment);

			var modules = ModuleCollection.New()
				.AddBasicModules(_hostingEnvironment);

			var bootstrapper = new Bootstrapper(Container);

			var assemblies = AssemblyCollection.New()
				.AddEntryAssembly()
				.AddModules(Environment.CurrentDirectory);

			var resolver = bootstrapper.Initialize(assemblies, modules);

			GlobalServiceResolver.SetResolver(resolver);

			var appBuilder = new ApplicationBuilder(resolver, _hostingEnvironment);
			appBuilder.UseBasicModules();
			appBuilder.Build();
		}
	}
}
