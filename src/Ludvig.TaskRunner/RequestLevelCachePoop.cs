﻿using System;
using System.Collections.Generic;
using Ludvig.Utilities.Caching;

namespace Ludvig.TaskRunner
{
	public class RequestLevelCacher : IRequestLevelCacher
	{
		public ICollection<T> Get<T>(IEnumerable<string> keys)
		{
			throw new NotImplementedException();
		}

		public void Save<T>(string key, T item, TimeSpan? cacheDuration = null)
		{
			throw new NotImplementedException();
		}

		public void Invalidate(IEnumerable<string> keys)
		{
			throw new NotImplementedException();
		}

		public void InvalidateAll()
		{
			throw new NotImplementedException();
		}
	}
}