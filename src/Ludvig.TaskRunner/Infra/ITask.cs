﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ludvig.Utilities.Ioc;

namespace Ludvig.TaskRunner.Infra
{
	[RegisterAll]
	public interface ITask
	{
		void Run(string[] args);
	}

	public static class TaskRunnerTaskExtensions
	{
		public static void RunTask<T>(this IEnumerable<ITask> target, params string[] args) where T : ITask
		{
			var task = target.OfType<T>().Single();
			Console.WriteLine($"Running {task.GetType().Name}");
			try
			{
				task.Run(args);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error: " + ex.Message);
				Console.WriteLine(ex);
			}

			Console.WriteLine($"Finished {task.GetType().Name}");
		}
	}
}