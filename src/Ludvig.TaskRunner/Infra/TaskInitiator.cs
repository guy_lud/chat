﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ludvig.Utilities.Defence;
using Ludvig.Utilities.Logging;

namespace Ludvig.TaskRunner.Infra
{
	public interface ITaskInitiator
	{
		void Run(string[] args);
	}

	public class TaskInitiator : ITaskInitiator
	{
		private readonly ITask[] _tasks;
		private readonly ILogger _logger;

		public TaskInitiator(IEnumerable<ITask> tasks, ILogger logger)
		{
			_logger = logger;
			_tasks = tasks.ToArray();
		}

		public void Run(string[] args)
		{
			if (args.IsNullOrEmpty())
				throw new InvalidOperationException();

			var taskName = args[0];
			taskName = taskName.EndsWith("task", StringComparison.OrdinalIgnoreCase) ? taskName : taskName + "Task";
			var taskArgs = args.Skip(1).ToArray();

			var task = _tasks.FirstOrDefault(x => x.GetType().Name.Equals(taskName, StringComparison.OrdinalIgnoreCase));
			if (task == null)
				throw new ApplicationException($"Couldn't find task [{taskName}]");

			RunTask(task, taskArgs);
		}

		private void RunTask(ITask task, string[] taskArgs)
		{
			try
			{
				Console.WriteLine($"===== Running {task.GetType().Name} =====");
				task.Run(taskArgs);
			}
			catch (Exception ex)
			{
				_logger.LogCritical(ex.ToString());
			}

			Console.WriteLine($"===== Finished {task.GetType().Name} =====");
		}
	}
}