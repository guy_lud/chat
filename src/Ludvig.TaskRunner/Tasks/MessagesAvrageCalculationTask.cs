﻿using System.Collections.Generic;
using System.Linq;
using Ludvig.Modules.Contracts.Messages;
using Ludvig.Modules.Contracts.Reports;
using Ludvig.TaskRunner.Infra;

namespace Ludvig.TaskRunner.Tasks
{
	internal class MessagesAvrageCalculationTask : ITask
	{
		private readonly IMessageProvider _messageProvider;
		private readonly IReportRepository _reportRepository;

		public MessagesAvrageCalculationTask(IMessageProvider messageProvider,
			IReportRepository reportRepository)
		{
			_messageProvider = messageProvider;
			_reportRepository = reportRepository;
		}

		public void Run(string[] args)
		{
			// i know that i need to use incremental avarage as i used on time avrage, but simply no time
			var report = CalculateNewReport();
				
			if(report == null)
				return;

			_reportRepository.SaveReport(report);
		}

		private MessagesAvrageLengthReport CalculateNewReport()
		{
			var messages = _messageProvider.FetchAllMessages().ToArray();

			return CalculateTotalAvrage(messages);
		}

		private MessagesAvrageLengthReport CalculateTotalAvrage(Message[] messages)
		{
			var dic = new Dictionary<string, List<int>>();
			int total = 0;

			if (!messages.Any())
				return null;

			foreach (var message in messages)
			{
				var userId = message.SenderId;
				var count = message.Content.Count(char.IsLetter);

				total += count;

				if (dic.ContainsKey(userId))
					dic[userId].Add(count);
				else
					dic.Add(userId, new List<int>() { count });
			}

			return new MessagesAvrageLengthReport()
			{
				Avg = total / messages.Count(),
				LastCount = messages.Count(),
				LastId = messages.Max(x => x.Id),
				UserMessageWordsAvragesCollection = dic.Select(x => new UserMessageWordsAvrage()
				{
					LastAvrage = (float)x.Value.Average(),
					LastCount = x.Value.Count,
					UserId = x.Key
				}).ToList()
			};
		}
	}
}