﻿using System;
using System.Collections.Generic;
using Ludvig.TaskRunner.Infra;

namespace Ludvig.TaskRunner.Tasks
{
	public class CalculatingMasterTask : ITask
	{
		private readonly IList<ITask> _tasks;

		public CalculatingMasterTask(IList<ITask> tasks)
		{
			_tasks = tasks;
		}

		public void Run(string[] args)
		{
			_tasks.RunTask<TimeBetweenMessagesAvarageCalculationTask>();
			_tasks.RunTask<MessagesAvrageCalculationTask>();
		}
	}
}