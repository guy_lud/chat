﻿using System.Collections.Generic;
using System.Linq;
using Ludvig.Modules.Contracts.Messages;
using Ludvig.Modules.Contracts.Reports;
using Ludvig.TaskRunner.Infra;

namespace Ludvig.TaskRunner.Tasks
{
	internal class TimeBetweenMessagesAvarageCalculationTask : ITask
	{
		private readonly IMessageProvider _messageProvider;
		private readonly IReportRepository _reportRepository;

		public TimeBetweenMessagesAvarageCalculationTask(IMessageProvider messageProvider,
			IReportRepository reportRepository)
		{
			_messageProvider = messageProvider;
			_reportRepository = reportRepository;
		}

		public void Run(string[] args)
		{
			var existingReport = _reportRepository.GetReport<MessageAvrageTimeReport>();

			var report = existingReport == null ?CalculateNewReport() : CalculateMovingReport(existingReport);

			if(report == null)
				return;
			
			_reportRepository.SaveReport(report);
		}

		private MessageAvrageTimeReport CalculateMovingReport(Report<MessageAvrageTimeReport> existingReport)
		{
			var report = existingReport.Data;

			var messages = _messageProvider.FetchDeltaMessages(report.LastId)
				.ToArray();

			var avg = report.Avg;
			var count = report.LastCount;

			var timeSpans = CalculateTimeSpan(messages);

			for (int i = 0; i < timeSpans.Length ; i++)
			{
				if (i == timeSpans.Length - 1)
					break;

				avg = avg + ((timeSpans[i] - avg)/++count);
			}

			return new MessageAvrageTimeReport()
			{
				Avg =  avg,
				LastCount = count,
				LastId = messages.Last().Id
			};
		}

		private MessageAvrageTimeReport CalculateNewReport()
		{
			var messages = _messageProvider.FetchAllMessages()
				.OrderBy(x=>x.TimeCreated);

			var last = messages.Last();

			var timeSpans = CalculateTimeSpan(messages);

			var avr = timeSpans.Average();

			return new MessageAvrageTimeReport()
			{
				LastCount = timeSpans.Length,
				Avg = (long)avr,
				LastId = last.Id
			};
		}

		private long[] CalculateTimeSpan(IEnumerable<Message> messages)
		{
			var ticks = messages.OrderBy(x => x.TimeCreated)
				.Select(x => x.TimeCreated.Ticks).ToArray();

			var count = ticks.Count();
			var array = new long[count-1];


			for (int i = 0; i < count; i++)
			{
				if (i == count - 1)
					break;

				array[i] = ticks[i + 1] - ticks[i];
			}

			return array;
		}
	}
}
