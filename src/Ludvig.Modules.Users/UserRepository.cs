﻿using System.Linq;
using System.Threading.Tasks;
using Hermes.DataAccess.Databases;
using Hermes.Modules.Contracts.Users;

namespace Hermes.Modules.Users
{
	class UserRepository : IUserRepository
	{
		private readonly IWarehouse<UserDto> _warehouse;

		public UserRepository(IWarehouse<UserDto> warehouse)
		{
			_warehouse = warehouse;
		}

		public Task<User> GetUserAsync(UserId userId)
		{
			return _warehouse.GetByIdAsync(userId)
				.ContinueWith(x => x.Result == null ? null : Map(x.Result));
		}

		public Task UpdateUserAsync(User user)
		{
			var dto = new UserDto
			{
				Id = user.Id,
				Email = user.Email,
				HashedPassword = user.Password,
				OAuthes = user.OAuthes.ToArray(),
				IsVerified = user.IsVerified
			};

			return _warehouse.UpdateAsync(dto);
		}

		private static User Map(UserDto source)
		{
			return new User
			{
				Id = source.Id,
				Email = source.Email,
				Password = source.HashedPassword,
				OAuthes = source.OAuthes.ToList(),
				IsVerified = source.IsVerified
			};
		}
	}
}
