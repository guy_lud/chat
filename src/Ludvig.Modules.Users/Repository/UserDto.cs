﻿using Ludvig.Modules.Contracts.Entities;
using SQLite;

namespace Ludvig.Modules.Users.Repository
{
	[Table("Users")]
	public class UserDto : IEntity<string>
	{
		public string Id { get; set; }
		public string UserName { get; set; }
	}
}
