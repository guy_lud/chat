using System;
using Ludvig.DataAccess.Databases;
using Ludvig.Modules.Contracts.Users;

namespace Ludvig.Modules.Users.Repository
{
	class UsersCreator : IUsersCreator
	{
		private readonly IWarehouse<UserDto> _users;
		

		public UsersCreator(IWarehouse<UserDto> users)
		{
			_users = users;
		}

		public void SignUp(string userName)
		{
			var user = new UserDto
			{
				Id = Guid.NewGuid().ToString(),
				UserName = userName
			};
			
			try
			{
				_users.Add(user);
			}
			catch (Exception e)
			{
				throw new UserAlreadyExistsAuthenticationException();
			}
		}
	}
}