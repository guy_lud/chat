using Ludvig.Modules.Contracts.Users;
using Ludvig.Utilities.Mapping;

namespace Ludvig.Modules.Users.Repository
{
	internal class UserMapper : IBindingMapper
	{
		public void Bind(IBindingContex contex)
		{
			contex.Bind<User, UserDto>();

			contex.Bind<UserDto, User>();
		}
	}
}