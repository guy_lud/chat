using Ludvig.Modules.Contracts.Users;
using Ludvig.Utilities.Mapping;

namespace Ludvig.Modules.Users.Repository
{
	class UserIdMapper : IBindingMapper
	{
		public void Bind(IBindingContex contex)
		{
			contex.SimpleBind<UserId, string>(userId => (string) userId);
			contex.SimpleBind<string, UserId>(str => (UserId)str);
		}
	}
}