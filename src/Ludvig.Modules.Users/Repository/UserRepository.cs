﻿using System.Linq;
using Ludvig.DataAccess.Databases;
using Ludvig.Modules.Contracts.Users;
using Ludvig.Utilities.Mapping;

namespace Ludvig.Modules.Users.Repository
{
	class UserRepository : IUserRepository
	{
		private readonly IWarehouse<UserDto> _warehouse;
		private readonly IObjectMapper _objectMapper;

		public UserRepository(IWarehouse<UserDto> warehouse, IObjectMapper objectMapper)
		{
			_warehouse = warehouse;
			_objectMapper = objectMapper;
		}

		public User GetUserById(string userId)
		{
			var dto = _warehouse.Query(x => x.Where(y => y.Id == userId)).FirstOrDefault();
			return _objectMapper.Map<UserDto, User>(dto);
		}

		public User GetUserByName(string userName)
		{
			var dto = _warehouse.Query(x => x.Where(y => y.UserName == userName)).FirstOrDefault();
			return _objectMapper.Map<UserDto, User>(dto);
		}

		public void UpdateUser(User user)
		{
			var dto = _objectMapper.Map<User, UserDto>(user);

			_warehouse.Update(dto);
		}
	}
}
