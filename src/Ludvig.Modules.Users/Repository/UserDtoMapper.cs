﻿//using System.Linq;
//using Ludvig.Modules.Contracts.System.StaticData;
//using Ludvig.Modules.Contracts.Users;
//using Ludvig.Modules.Contracts.Users.Authorization;
//using Ludvig.Utilities.Crypto;

//namespace Ludvig.Modules.Users.Repository
//{
//	interface IUserDtoMapper
//	{
//		UserDto Map(User user);
//		User Map(UserDto dto);
//	}

//	public class UserDtoMapper : IUserDtoMapper
//	{
//		private readonly IAesEncryptor _aesEncryptor;
//		private readonly IStaticDataProvider _staticDataProvider;

//		public UserDtoMapper(IAesEncryptor aesEncryptor, IStaticDataProvider staticDataProvider)
//		{
//			_aesEncryptor = aesEncryptor;
//			_staticDataProvider = staticDataProvider;
//		}

//		public UserDto Map(User user)
//		{
//			if (user == null)
//				return null;

//			var dto = new UserDto
//			{
//				Id = user.Id,
//				Email = user.Email,
//				HashedPassword = user.Password,
//				NormalizeEmail = user.NormalizeEmail,
//				IsVerified = user.IsVerified,
//				OAuthes = user.OAuthes.ToArray(),
//				ManagedAdWordsAccountHierarchy = user.ManagedAdWordsAccountHierarchy,
//				SelectedAdWordsAccountId = user.SelectedAdWordsAccountId,
//				NetworkAuths = user.NetworkAuths.ToArray(),
//				Roles = user.Roles.Select(x => x.Id).ToArray()
//			};

//			foreach (var oAuth in dto.OAuthes)
//			{
//				oAuth.Token = _aesEncryptor.Encrypt(oAuth.Token);
//				oAuth.RefreshToken = _aesEncryptor.Encrypt(oAuth.RefreshToken);
//			}

//			return dto;
//		}

//		public User Map(UserDto dto)
//		{
//			if (dto == null)
//				return null;

//			var user = new User
//			{
//				Id = dto.Id,
//				Email = dto.Email,
//				NormalizeEmail = dto.NormalizeEmail,
//				Password = dto.HashedPassword,
//				IsVerified = dto.IsVerified,
//				ManagedAdWordsAccountHierarchy = dto.ManagedAdWordsAccountHierarchy,
//				SelectedAdWordsAccountId = dto.SelectedAdWordsAccountId,
//				OAuthes = dto.OAuthes.ToList(),
//				NetworkAuths = dto.NetworkAuths.ToList()
//			};

//			foreach (var oAuth in dto.OAuthes)
//			{
//				oAuth.Token = _aesEncryptor.DecryptOrNull(oAuth.Token);
//				oAuth.RefreshToken = _aesEncryptor.DecryptOrNull(oAuth.RefreshToken);
//			}

//			MapRoles(user, dto.Roles);

//			return user;
//		}

//		private void MapRoles(User user, long[] rolesIds)
//		{
//			var roles = _staticDataProvider.GetRoles()?.Where(role => rolesIds.Contains(role.Id));
//			user.Roles.AddRange(roles ?? new Role[0]);
//		}
//	}
//}