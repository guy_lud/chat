using System;
using System.Linq;
using Ludvig.DataAccess.Databases;
using Ludvig.Modules.Contracts.Users;
using Ludvig.Utilities.Crypto;
using Ludvig.Utilities.Defence;

namespace Ludvig.Modules.Users.Authentication
{
	public class UserAuthenticator : IUserAuthenticator
	{
		private readonly IWarehouse<SessionDto> _sessions;
		private readonly IPasswordHasher _passwordHasher;
		private readonly IUserRepository _userRepository;

		public UserAuthenticator(IWarehouse<SessionDto> sessions,
			IPasswordHasher passwordHasher, IUserRepository userRepository)
		{
			_sessions = sessions;
			_passwordHasher = passwordHasher;
			_userRepository = userRepository;
		}

		public string Login(string userName)
		{
			userName.ThrowIfNull();

			var user =  _userRepository.GetUserByName(userName);

			if (user == null)
				throw new UserDoesntExistsAuthenticationException();

			var sessionId = Guid.NewGuid().ToShortKey();
			var session = new SessionDto
			{
				Id = sessionId,
				Expired = DateTime.UtcNow + TimeSpan.FromDays(30),
				UserId = user.Id
			};

			_sessions.Add(session);

			return sessionId;
		}

		public User GetSessionUser(string sessionId)
		{
			var session = _sessions.Query(x=>x.Where(q=>q.Id == sessionId)).FirstOrDefault();
			if (session == null)
				return null;

			return _userRepository.GetUserById(session.UserId);
		}
	}
}