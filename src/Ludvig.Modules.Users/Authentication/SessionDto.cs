using System;
using Ludvig.Modules.Contracts.Entities;
using SQLite;

namespace Ludvig.Modules.Users.Authentication
{
	[Table("Sessions")]
	public class SessionDto : IEntity<string>
	{
		[PrimaryKey]
		public string Id { get; set; }
		public string UserId { get; set; }

		public DateTime Expired { get; set; }
	}
}