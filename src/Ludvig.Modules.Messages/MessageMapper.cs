﻿using Ludvig.Modules.Contracts.Messages;
using Ludvig.Utilities.Mapping;

namespace Ludvig.Modules.Messages
{
	internal class MessageMapper : IBindingMapper
	{
		public void Bind(IBindingContex contex)
		{
			contex.Bind<MessageDto, Message>();
			contex.Bind<Message, MessageDto>();
		}
	}
}