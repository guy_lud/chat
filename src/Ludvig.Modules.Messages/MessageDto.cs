﻿using System;
using SQLite;

namespace Ludvig.Modules.Messages
{
	[Table("Messages")]
	internal class MessageDto
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }
		public string SenderId { get; set; }
		public DateTime TimeCreated { get; set; }
		public string Content { get; set; }
	}
}