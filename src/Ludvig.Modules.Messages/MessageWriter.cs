﻿using Ludvig.Modules.Contracts.Messages;

namespace Ludvig.Modules.Messages
{
	internal class MessageWriter : IMessageWriter
	{
		private readonly IMessageRepository _messageRepository;

		public MessageWriter(IMessageRepository messageRepository)
		{
			_messageRepository = messageRepository;
		}

		public int WriteMessgae(string userId, string content)
		{
			var id = _messageRepository.SaveNewMessage(userId, content);

			return id;
		}
	}
}