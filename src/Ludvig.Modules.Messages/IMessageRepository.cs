﻿using System.Collections.Generic;
using System.Linq;
using Ludvig.DataAccess.Databases;
using Ludvig.Modules.Contracts.Messages;
using Ludvig.Utilities.DateTime;
using Ludvig.Utilities.Mapping;

namespace Ludvig.Modules.Messages
{
	internal interface IMessageRepository
	{
		IEnumerable<Message> GetAllMessages();
		IEnumerable<Message> GetMessages(int fromId);
		int SaveNewMessage(string userId, string text);
	}

	class MessageRepository : IMessageRepository
	{
		private readonly IWarehouse<MessageDto> _warehouse;
		private readonly IObjectMapper _objectMapper;
		private readonly ISystemTime _systemTime;

		public MessageRepository(IWarehouse<MessageDto> warehouse,
			IObjectMapper objectMapper,
			ISystemTime systemTime)
		{
			_warehouse = warehouse;
			_objectMapper = objectMapper;
			_systemTime = systemTime;
		}

		public IEnumerable<Message> GetAllMessages()
		{
			return _warehouse.QueryAll()
				.Select(x => _objectMapper.Map<MessageDto, Message>(x)).ToArray();
		}

		public IEnumerable<Message> GetMessages(int fromId)
		{
			return _warehouse.Query(x=>x.Where(q=>q.Id > fromId))
				.Select(x => _objectMapper.Map<MessageDto, Message>(x)).ToArray();
		}

		public int SaveNewMessage(string userId, string text)
		{
			var dto = new MessageDto()
			{
				Content = text,
				SenderId = userId,
				TimeCreated = _systemTime.Now
			};

			_warehouse.Add(dto);

			return dto.Id; 
		}
	}
}