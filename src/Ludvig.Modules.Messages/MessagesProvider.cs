﻿using System.Collections.Generic;
using Ludvig.Modules.Contracts.Messages;

namespace Ludvig.Modules.Messages
{
	internal class MessageProvider : IMessageProvider
	{
		private readonly IMessageRepository _repository;

		public MessageProvider(IMessageRepository repository)
		{
			_repository = repository;
		}

		public IEnumerable<Message> FetchAllMessages()
		{
			return _repository.GetAllMessages();
		}

		public IEnumerable<Message> FetchDeltaMessages(int lastId)
		{
			return _repository.GetMessages(lastId);
		}
	}
}
